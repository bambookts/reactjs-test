# Frontend Task

## Task
Write an app to display our catalog of products (similar to shopify).

Search is an important functionality that we wish to improve on. Based on the available product data, add to your catalog a search functionality using an algorigthm that you judge would bring up the most relevant result for a given query.

## Getting the data

### Products

The products data is available in `products-data.js` as a promise that will take 2s to resolve (micking fetching the products with a network request).

Note on the product data:
- `price` is cents and Canadian Dollards (CAD)
- `available` represent weither the item still has inventory

## Getting started

To help you out the project scaffolding with React, CSS, bundling (Parcel), Babel (ES6-ES7), and hot module reloading is already setup

To install the dependancies
```
npm install
```

To run your app
```
npm run start
```
