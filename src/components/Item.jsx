import React from 'react';

import {Navbar, Nav, Form, FormControl, Button, Glyphicon} from 'react-bootstrap';

class Item extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <ul>
                <li>Id: {this.props.item.id}</li>
                <li>Description: {this.props.item.description}</li>
                <li>Title: {this.props.item.title}</li>
                <li>Handle: {this.props.item.handle}</li>
                <li>Available: {this.props.item.available}</li>
                <li>Price: {this.props.item.price}</li>
            </ul>
        )
    }
}

export default Item;