import React from 'react';

import {Navbar, Nav, Form, FormControl, Button, Glyphicon} from 'react-bootstrap';
import getProducts from "../getProducts";
import Item from './Item';



class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false
        };

    }

    componentDidMount() {
        this.loadItems();
    }

    loadItems() {
        getProducts().then((data) => {
            console.log("data", data);
            this.setState({
                isLoaded: true,
                items: data
            })
        });
    }

    render() {
        return (
            <div className="test1">
                {this.state.items.map((data)=>{
                    return <Item item = {data} />
                })}
            </div>
        );
    }
}
export default Body;