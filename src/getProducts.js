export default () => new Promise(resolve =>
  setTimeout(
    () =>
      resolve([
        {
          id: "3955165003818",
          description:
            "Floral Lace Tights are anything but boring! These tights make the outfit thanks to their beautiful floral lace finish. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty boxer Micro-fishnet finish Colour: black",
          title: "Floral Lace Tights",
          handle: "floral-lace-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Lace_Tights_560x.jpg?v=1571172153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Lace_Tights_720x.jpg?v=1571172153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Lace_Tights_966x.jpg?v=1571172153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Lace_Tights_1200x.jpg?v=1571172153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Lace_Tights_small.jpg?v=1571172153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/FloralLaceTights_560x.jpg?v=1571172153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/FloralLaceTights_720x.jpg?v=1571172153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/FloralLaceTights_966x.jpg?v=1571172153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/FloralLaceTights_1200x.jpg?v=1571172153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/FloralLaceTights_small.jpg?v=1571172153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/P1260311_560x.jpg?v=1571341145",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/P1260311_720x.jpg?v=1571341145",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/P1260311_966x.jpg?v=1571341145",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/P1260311_1200x.jpg?v=1571341145",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/P1260311_small.jpg?v=1571341145"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3955178078250",
          description:
            "These warm tights will add dimension to all your looks with their weave finish. It’s a must-have tights option for the winter months!Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXColour: Black",
          title: "Warm Black Weave Tights",
          handle: "black-opaque-weave-tights",
          available: true,
          price: 1700,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Black_tights_560x.jpg?v=1571172531",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Black_tights_720x.jpg?v=1571172531",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Black_tights_966x.jpg?v=1571172531",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Black_tights_1200x.jpg?v=1571172531",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Black_tights_small.jpg?v=1571172531"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3955181420586",
          description:
            "Florals aren’t just for spring anymore thanks to these Floral Side Print Tights. They’re sexy, yet sophisticated with a sheer finish that will make heads turn. Made in Italy1 inch waistband Standard 100 certified by OEKO-TEXColour: black",
          title: "Floral Side Print Tights",
          handle: "floral-side-print-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Side_Print_Tights_560x.jpg?v=1571172185",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Side_Print_Tights_720x.jpg?v=1571172185",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Side_Print_Tights_966x.jpg?v=1571172185",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Side_Print_Tights_1200x.jpg?v=1571172185",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Floral_Side_Print_Tights_small.jpg?v=1571172185"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_560x.jpg?v=1571172197",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_720x.jpg?v=1571172197",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_966x.jpg?v=1571172197",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_1200x.jpg?v=1571172197",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_small.jpg?v=1571172197"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3951562817578",
          description:
            "Embrace your wild side with Leopard Print Tights. This is a trendy hosiery option that will add some fun to your wardrobe and get your legs noticed.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty boxerMicro-fishnet finish Colour: black",
          title: "Leopard Print Tights",
          handle: "leopard-print-tight",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-leopard_560x.jpg?v=1571776602",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-leopard_720x.jpg?v=1571776602",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-leopard_966x.jpg?v=1571776602",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-leopard_1200x.jpg?v=1571776602",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-leopard_small.jpg?v=1571776602"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_560x.jpg?v=1571776602",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_720x.jpg?v=1571776602",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_966x.jpg?v=1571776602",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_1200x.jpg?v=1571776602",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_small.jpg?v=1571776602"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_ec77a8b9-c955-4eac-9e66-d72218032ae2_560x.jpg?v=1571776602",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_ec77a8b9-c955-4eac-9e66-d72218032ae2_720x.jpg?v=1571776602",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_ec77a8b9-c955-4eac-9e66-d72218032ae2_966x.jpg?v=1571776602",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_ec77a8b9-c955-4eac-9e66-d72218032ae2_1200x.jpg?v=1571776602",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leopard_Print_Tights_ec77a8b9-c955-4eac-9e66-d72218032ae2_small.jpg?v=1571776602"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3955179487274",
          description:
            "These warm tights will add dimension to all your looks with their weave finish. It’s a must-have tights option for the winter months!Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXColour: Burgundy",
          title: "Warm Burgundy Weave Tights",
          handle: "burgundy-opaque-weave-tights",
          available: true,
          price: 1700,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Burgundy_Weave_Tights_560x.jpg?v=1571328490",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Burgundy_Weave_Tights_720x.jpg?v=1571328490",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Burgundy_Weave_Tights_966x.jpg?v=1571328490",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Burgundy_Weave_Tights_1200x.jpg?v=1571328490",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Burgundy_Weave_Tights_small.jpg?v=1571328490"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_bordeaux_560x.jpg?v=1571338863",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_bordeaux_720x.jpg?v=1571338863",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_bordeaux_966x.jpg?v=1571338863",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_bordeaux_1200x.jpg?v=1571338863",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_bordeaux_small.jpg?v=1571338863"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_automne_560x.jpg?v=1571338863",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_automne_720x.jpg?v=1571338863",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_automne_966x.jpg?v=1571338863",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_automne_1200x.jpg?v=1571338863",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_automne_small.jpg?v=1571338863"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/burgundy_tights_560x.jpg?v=1571338863",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/burgundy_tights_720x.jpg?v=1571338863",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/burgundy_tights_966x.jpg?v=1571338863",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/burgundy_tights_1200x.jpg?v=1571338863",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/burgundy_tights_small.jpg?v=1571338863"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3955166412842",
          description:
            "Amplify your looks even on the coldest days with these opaque tights featuring an intricate floral side print finish. It’s a small detail that will go a long way.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty boxerColour: black",
          title: "Opaque Floral Side Print Tights",
          handle: "opaque-floral-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Tights_560x.jpg?v=1571172423",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Tights_720x.jpg?v=1571172423",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Tights_966x.jpg?v=1571172423",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Tights_1200x.jpg?v=1571172423",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Tights_small.jpg?v=1571172423"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Side_Print_Tights_560x.jpg?v=1571172423",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Side_Print_Tights_720x.jpg?v=1571172423",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Side_Print_Tights_966x.jpg?v=1571172423",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Side_Print_Tights_1200x.jpg?v=1571172423",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Opaque_Floral_Side_Print_Tights_small.jpg?v=1571172423"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1905351491626",
          description:
            "These sheer tights feature floral detailing across the lower leg for an eye-catching finish that’s perfect for spring. Made in Italy Comfortable and resistantMicromeshPanty With cotton gusset Flat seam30 DeniersColour : Black",
          title: "Tights with Floral Detail",
          handle: "tights-with-floral-detail",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights2_560x.jpg?v=1571344137",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights2_720x.jpg?v=1571344137",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights2_966x.jpg?v=1571344137",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights2_1200x.jpg?v=1571344137",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights2_small.jpg?v=1571344137"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights_560x.jpg?v=1571344137",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights_720x.jpg?v=1571344137",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights_966x.jpg?v=1571344137",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights_1200x.jpg?v=1571344137",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights_small.jpg?v=1571344137"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights3_560x.jpg?v=1567700940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights3_720x.jpg?v=1567700940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights3_966x.jpg?v=1567700940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights3_1200x.jpg?v=1567700940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/floraltights3_small.jpg?v=1567700940"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 12
        },

        {
          id: "3955170312234",
          description:
            "Rachel is bringing back the classic houndstooth print in one fun semi-sheer tights option. They were handpicked by Rachel for evening and day! Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty boxerMicro-fishnet finish Colour: black",
          title: "Houndstooth Print Tights",
          handle: "houndstooth-print-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights2_560x.jpg?v=1571844207",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights2_720x.jpg?v=1571844207",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights2_966x.jpg?v=1571844207",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights2_1200x.jpg?v=1571844207",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights2_small.jpg?v=1571844207"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_3abe4e2a-d71f-463f-ba68-210fde10b51c_560x.jpg?v=1571844207",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_3abe4e2a-d71f-463f-ba68-210fde10b51c_720x.jpg?v=1571844207",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_3abe4e2a-d71f-463f-ba68-210fde10b51c_966x.jpg?v=1571844207",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_3abe4e2a-d71f-463f-ba68-210fde10b51c_1200x.jpg?v=1571844207",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_3abe4e2a-d71f-463f-ba68-210fde10b51c_small.jpg?v=1571844207"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_560x.jpg?v=1571844207",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_720x.jpg?v=1571844207",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_966x.jpg?v=1571844207",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_1200x.jpg?v=1571844207",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Print_Tights_small.jpg?v=1571844207"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_motif_560x.jpg?v=1571338971",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_motif_720x.jpg?v=1571338971",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_motif_966x.jpg?v=1571338971",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_motif_1200x.jpg?v=1571338971",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_motif_small.jpg?v=1571338971"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Tights_560x.jpg?v=1571338971",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Tights_720x.jpg?v=1571338971",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Tights_966x.jpg?v=1571338971",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Tights_1200x.jpg?v=1571338971",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Houndstooth_Tights_small.jpg?v=1571338971"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "806600572970",
          description:
            "Over-the-knee socks are a wardrobe staple for the cold months and these ones made of high quality fabrics are sure to keep you warm. Their soft fabric will stretch to your leg shape and don’t look bulky!Note: fits maximum thigh size 16 inches; calf size 16 inches",
          title: "Over-The-Knee Socks",
          handle: "over-the-knee-socks",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_sock_560x.jpg?v=1571340109",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_sock_720x.jpg?v=1571340109",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_sock_966x.jpg?v=1571340109",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_sock_1200x.jpg?v=1571340109",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_sock_small.jpg?v=1571340109"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bashaut_haut_560x.jpg?v=1572033273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bashaut_haut_720x.jpg?v=1572033273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bashaut_haut_966x.jpg?v=1572033273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bashaut_haut_1200x.jpg?v=1572033273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bashaut_haut_small.jpg?v=1572033273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/hight_sock2_560x.jpg?v=1572033273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/hight_sock2_720x.jpg?v=1572033273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/hight_sock2_966x.jpg?v=1572033273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/hight_sock2_1200x.jpg?v=1572033273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/hight_sock2_small.jpg?v=1572033273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_socks_560x.jpg?v=1572033273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_socks_720x.jpg?v=1572033273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_socks_966x.jpg?v=1572033273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_socks_1200x.jpg?v=1572033273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_socks_small.jpg?v=1572033273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_socks_copie_560x.jpg?v=1572033273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_socks_copie_720x.jpg?v=1572033273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_socks_copie_966x.jpg?v=1572033273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_socks_copie_1200x.jpg?v=1572033273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/high_socks_copie_small.jpg?v=1572033273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/OTKSOCKS_560x.jpg?v=1572033273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/OTKSOCKS_720x.jpg?v=1572033273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/OTKSOCKS_966x.jpg?v=1572033273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/OTKSOCKS_1200x.jpg?v=1572033273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/OTKSOCKS_small.jpg?v=1572033273"
            }
          ],
          type: "Socks",
          average_rating: 4.0,
          reviews_count: 33
        },

        {
          id: "806417039402",
          description:
            "A pair of tights covered in tiny hearts is the perfect accessory to add a touch of romance to your look. It’s love at first sight! Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset30 DeniersColor : black",
          title: "Heart Print Tights",
          handle: "heart-print-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_tights_560x.jpg?v=1571339371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_tights_720x.jpg?v=1571339371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_tights_966x.jpg?v=1571339371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_tights_1200x.jpg?v=1571339371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_tights_small.jpg?v=1571339371"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_print_tights_560x.jpg?v=1571339371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_print_tights_720x.jpg?v=1571339371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_print_tights_966x.jpg?v=1571339371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_print_tights_1200x.jpg?v=1571339371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/heart_print_tights_small.jpg?v=1571339371"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_560x.jpg?v=1571339371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_720x.jpg?v=1571339371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_966x.jpg?v=1571339371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_1200x.jpg?v=1571339371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_small.jpg?v=1571339371"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_f0fd463d-571e-4674-bfc3-57088d59d213_560x.jpg?v=1571339371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_f0fd463d-571e-4674-bfc3-57088d59d213_720x.jpg?v=1571339371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_f0fd463d-571e-4674-bfc3-57088d59d213_966x.jpg?v=1571339371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_f0fd463d-571e-4674-bfc3-57088d59d213_1200x.jpg?v=1571339371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurs_f0fd463d-571e-4674-bfc3-57088d59d213_small.jpg?v=1571339371"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_17c8437d-3a66-4681-887a-fbdede804e1f_560x.jpg?v=1571339371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_17c8437d-3a66-4681-887a-fbdede804e1f_720x.jpg?v=1571339371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_17c8437d-3a66-4681-887a-fbdede804e1f_966x.jpg?v=1571339371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_17c8437d-3a66-4681-887a-fbdede804e1f_1200x.jpg?v=1571339371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_17c8437d-3a66-4681-887a-fbdede804e1f_small.jpg?v=1571339371"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 131
        },

        {
          id: "1761226489898",
          description:
            "Over-the-knee tights are upgraded with flirty detailing this season; Rachel’s signature heart print. Located just above the knee, the heart stripe makes a subtle yet eye-catching difference. Made in Italy Comfortable and resistantBoxer brief  With cotton gussetAutomatic seams30 /60 DeniersColor : black",
          title: "Over-the-Knee Heart Stripe Tights",
          handle: "otkheart",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights2_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights2_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights2_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights2_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights2_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Heart_Stripe_Tights_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotkcoeur_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotkcoeur_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotkcoeur_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotkcoeur_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotkcoeur_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesocoeurs_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesocoeurs_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesocoeurs_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesocoeurs_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesocoeurs_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_70c8ad87-56a0-4be3-91d6-fe126e146555_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_70c8ad87-56a0-4be3-91d6-fe126e146555_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_70c8ad87-56a0-4be3-91d6-fe126e146555_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_70c8ad87-56a0-4be3-91d6-fe126e146555_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_70c8ad87-56a0-4be3-91d6-fe126e146555_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_195b606b-5518-44f6-9aa7-e7cef5f3c955_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_195b606b-5518-44f6-9aa7-e7cef5f3c955_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_195b606b-5518-44f6-9aa7-e7cef5f3c955_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_195b606b-5518-44f6-9aa7-e7cef5f3c955_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_195b606b-5518-44f6-9aa7-e7cef5f3c955_small.jpg?v=1571339940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_560x.jpg?v=1571339940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_720x.jpg?v=1571339940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_966x.jpg?v=1571339940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1200x.jpg?v=1571339940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_small.jpg?v=1571339940"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 58
        },

        {
          id: "1622817865770",
          description:
            "Our best-selling over-the-knee tights have been reinvented to include both stripes and polka dots. When combined, stripes and polka dots pack a lot of personality for a look that is sure to make heads turn.Made in ItalyStandard 100 certified by OEKO-TEXBoxer brief  No gusset Automatic seams30 /60 Deniers Color : black",
          title: "Over-the-Knee Striped Polka Dot Tights",
          handle: "over-the-knee-striped-polka-dot-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_e6f4fc90-e800-47ff-b056-a97dabd435e8_560x.jpg?v=1571843363",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_e6f4fc90-e800-47ff-b056-a97dabd435e8_720x.jpg?v=1571843363",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_e6f4fc90-e800-47ff-b056-a97dabd435e8_966x.jpg?v=1571843363",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_e6f4fc90-e800-47ff-b056-a97dabd435e8_1200x.jpg?v=1571843363",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_e6f4fc90-e800-47ff-b056-a97dabd435e8_small.jpg?v=1571843363"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_7ee74b91-4451-492d-90c8-9064e966a35f_560x.jpg?v=1571843363",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_7ee74b91-4451-492d-90c8-9064e966a35f_720x.jpg?v=1571843363",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_7ee74b91-4451-492d-90c8-9064e966a35f_966x.jpg?v=1571843363",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_7ee74b91-4451-492d-90c8-9064e966a35f_1200x.jpg?v=1571843363",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_7ee74b91-4451-492d-90c8-9064e966a35f_small.jpg?v=1571843363"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights2_560x.jpg?v=1571843363",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights2_720x.jpg?v=1571843363",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights2_966x.jpg?v=1571843363",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights2_1200x.jpg?v=1571843363",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights2_small.jpg?v=1571843363"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_cuissardes_560x.jpg?v=1571843725",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_cuissardes_720x.jpg?v=1571843725",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_cuissardes_966x.jpg?v=1571843725",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_cuissardes_1200x.jpg?v=1571843725",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_cuissardes_small.jpg?v=1571843725"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_560x.jpg?v=1571843725",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_720x.jpg?v=1571843725",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_966x.jpg?v=1571843725",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_1200x.jpg?v=1571843725",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Over-the-Knee_Striped_Polka_Dot_Tights_small.jpg?v=1571843725"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkdotitghts_560x.jpg?v=1571843725",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkdotitghts_720x.jpg?v=1571843725",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkdotitghts_966x.jpg?v=1571843725",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkdotitghts_1200x.jpg?v=1571843725",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkdotitghts_small.jpg?v=1571843725"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_9094da31-cebe-4e71-9b69-d19bf174114a_560x.jpg?v=1571843725",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_9094da31-cebe-4e71-9b69-d19bf174114a_720x.jpg?v=1571843725",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_9094da31-cebe-4e71-9b69-d19bf174114a_966x.jpg?v=1571843725",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_9094da31-cebe-4e71-9b69-d19bf174114a_1200x.jpg?v=1571843725",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_9094da31-cebe-4e71-9b69-d19bf174114a_small.jpg?v=1571843725"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 130
        },

        {
          id: "1647732949034",
          description:
            "These tights are as original as they are versatile. Featuring three horizontal stripes that sit across your thighs, they’ll give you an original style that isn’t too over-the-top.  Made in Italy Boxer brief  No gusset Automatic seams30 DeniersColor : black",
          title: "Triple Striped Tights",
          handle: "triple-striped-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_fc4faed5-d0f6-49a0-86a0-ea0dd155c043_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_fc4faed5-d0f6-49a0-86a0-ea0dd155c043_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_fc4faed5-d0f6-49a0-86a0-ea0dd155c043_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_fc4faed5-d0f6-49a0-86a0-ea0dd155c043_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_fc4faed5-d0f6-49a0-86a0-ea0dd155c043_small.jpg?v=1571339258"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/stripes_tights_small.jpg?v=1571339258"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/print_tights_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/print_tights_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/print_tights_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/print_tights_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/print_tights_small.jpg?v=1571339258"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_757c60a2-e755-4820-b0eb-8e0e789b5523_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_757c60a2-e755-4820-b0eb-8e0e789b5523_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_757c60a2-e755-4820-b0eb-8e0e789b5523_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_757c60a2-e755-4820-b0eb-8e0e789b5523_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_757c60a2-e755-4820-b0eb-8e0e789b5523_small.jpg?v=1571339258"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_ad933880-e8cf-4e61-a26f-acfa0da0b64d_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_ad933880-e8cf-4e61-a26f-acfa0da0b64d_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_ad933880-e8cf-4e61-a26f-acfa0da0b64d_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_ad933880-e8cf-4e61-a26f-acfa0da0b64d_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_ad933880-e8cf-4e61-a26f-acfa0da0b64d_small.jpg?v=1571339258"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant3bandes_560x.jpg?v=1571339258",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant3bandes_720x.jpg?v=1571339258",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant3bandes_966x.jpg?v=1571339258",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant3bandes_1200x.jpg?v=1571339258",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant3bandes_small.jpg?v=1571339258"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 69
        },

        {
          id: "1725926178858",
          description:
            "Polka dots meet foliage prints in this original tights design. Both fun and elegant, their eye-catching finish can complete any day or evening look. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXMicro-fishnet stylePanty Flat seam With cotton gussetColor : black~Add these tights to any solid coloured dress and take your outfit to new heights. ",
          title: "Sheer Foliage Polka Dot Tights",
          handle: "leaves-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_feuilles_9b9556fe-2b43-4ec9-9dcb-5c91f5256001_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_feuilles_9b9556fe-2b43-4ec9-9dcb-5c91f5256001_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_feuilles_9b9556fe-2b43-4ec9-9dcb-5c91f5256001_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_feuilles_9b9556fe-2b43-4ec9-9dcb-5c91f5256001_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_feuilles_9b9556fe-2b43-4ec9-9dcb-5c91f5256001_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights2_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights2_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights2_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights2_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Sheer_Foliage_Polka_Dot_Tights2_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_feuilles_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_feuilles_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_feuilles_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_feuilles_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_feuilles_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille2_f87895c1-72e9-448a-b635-fb3d33a16f7d_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille2_f87895c1-72e9-448a-b635-fb3d33a16f7d_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille2_f87895c1-72e9-448a-b635-fb3d33a16f7d_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille2_f87895c1-72e9-448a-b635-fb3d33a16f7d_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille2_f87895c1-72e9-448a-b635-fb3d33a16f7d_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_cd621875-4e6c-48a9-aa2f-14fce47be2e7_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_cd621875-4e6c-48a9-aa2f-14fce47be2e7_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_cd621875-4e6c-48a9-aa2f-14fce47be2e7_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_cd621875-4e6c-48a9-aa2f-14fce47be2e7_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_cd621875-4e6c-48a9-aa2f-14fce47be2e7_small.jpg?v=1571843543"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_f216b923-7965-4c2a-ba61-844de024f047_560x.jpg?v=1571843543",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_f216b923-7965-4c2a-ba61-844de024f047_720x.jpg?v=1571843543",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_f216b923-7965-4c2a-ba61-844de024f047_966x.jpg?v=1571843543",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_f216b923-7965-4c2a-ba61-844de024f047_1200x.jpg?v=1571843543",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfeuille_f216b923-7965-4c2a-ba61-844de024f047_small.jpg?v=1571843543"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 140
        },

        {
          id: "732515663914",
          description:
            "Following our original Over-the-Knee Tights, discover our updated version featuring stripes! They'll give you a sporty chic look that won't go unnoticed. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamNo gusset30 / 60 DeniersColor : black",
          title: "Over-The-Knee Striped Tights",
          handle: "over-the-knee-striped",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_02f6f0fe-0b2c-405b-9a4e-d0d8a8caf19f_560x.jpg?v=1571343928",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_02f6f0fe-0b2c-405b-9a4e-d0d8a8caf19f_720x.jpg?v=1571343928",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_02f6f0fe-0b2c-405b-9a4e-d0d8a8caf19f_966x.jpg?v=1571343928",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_02f6f0fe-0b2c-405b-9a4e-d0d8a8caf19f_1200x.jpg?v=1571343928",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_02f6f0fe-0b2c-405b-9a4e-d0d8a8caf19f_small.jpg?v=1571343928"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_cuissardeBande-compressor_560x.jpg?v=1571343928",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_cuissardeBande-compressor_720x.jpg?v=1571343928",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_cuissardeBande-compressor_966x.jpg?v=1571343928",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_cuissardeBande-compressor_1200x.jpg?v=1571343928",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_cuissardeBande-compressor_small.jpg?v=1571343928"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesrayures_560x.jpg?v=1571343928",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesrayures_720x.jpg?v=1571343928",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesrayures_966x.jpg?v=1571343928",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesrayures_1200x.jpg?v=1571343928",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesrayures_small.jpg?v=1571343928"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkstripe_36ce9ed4-52dd-4c81-8eb0-8a4510c519a5_560x.jpg?v=1571343927",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkstripe_36ce9ed4-52dd-4c81-8eb0-8a4510c519a5_720x.jpg?v=1571343927",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkstripe_36ce9ed4-52dd-4c81-8eb0-8a4510c519a5_966x.jpg?v=1571343927",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkstripe_36ce9ed4-52dd-4c81-8eb0-8a4510c519a5_1200x.jpg?v=1571343927",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkstripe_36ce9ed4-52dd-4c81-8eb0-8a4510c519a5_small.jpg?v=1571343927"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_992b8c21-509c-40e0-840a-6db8de803fd7_560x.jpg?v=1571343927",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_992b8c21-509c-40e0-840a-6db8de803fd7_720x.jpg?v=1571343927",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_992b8c21-509c-40e0-840a-6db8de803fd7_966x.jpg?v=1571343927",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_992b8c21-509c-40e0-840a-6db8de803fd7_1200x.jpg?v=1571343927",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_992b8c21-509c-40e0-840a-6db8de803fd7_small.jpg?v=1571343927"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-rayures4_560x.jpg?v=1571343927",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-rayures4_720x.jpg?v=1571343927",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-rayures4_966x.jpg?v=1571343927",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-rayures4_1200x.jpg?v=1571343927",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-rayures4_small.jpg?v=1571343927"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 433
        },

        {
          id: "1836067651626",
          description:
            "Make a statement this season in these tights featuring an all-over white polka dot print that are packed with personality.Made in Italy Comfortable and resistant220 DeniersColour : Black",
          title: "Warm Black White Polka Dot Tights",
          handle: "warm-black-white-polka-dot-tights",
          available: true,
          price: 2000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights-2_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights-2_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights-2_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights-2_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights-2_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights1_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights1_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights1_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights1_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Warm_Black_White_Polka_Dot_Tights1_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_aefd7f0e-d4ed-48ef-aec6-78ec1c2d3358_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_aefd7f0e-d4ed-48ef-aec6-78ec1c2d3358_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_aefd7f0e-d4ed-48ef-aec6-78ec1c2d3358_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_aefd7f0e-d4ed-48ef-aec6-78ec1c2d3358_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_aefd7f0e-d4ed-48ef-aec6-78ec1c2d3358_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_54b5e8b1-69c6-4188-87c7-9ba8ad1fecf0_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_54b5e8b1-69c6-4188-87c7-9ba8ad1fecf0_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_54b5e8b1-69c6-4188-87c7-9ba8ad1fecf0_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_54b5e8b1-69c6-4188-87c7-9ba8ad1fecf0_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_54b5e8b1-69c6-4188-87c7-9ba8ad1fecf0_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2147bd24-6a6a-42c9-968b-15c8dbdac522_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2147bd24-6a6a-42c9-968b-15c8dbdac522_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2147bd24-6a6a-42c9-968b-15c8dbdac522_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2147bd24-6a6a-42c9-968b-15c8dbdac522_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2147bd24-6a6a-42c9-968b-15c8dbdac522_small.jpg?v=1571928259"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_560x.jpg?v=1571928259",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_720x.jpg?v=1571928259",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_966x.jpg?v=1571928259",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_1200x.jpg?v=1571928259",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_small.jpg?v=1571928259"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 51
        },

        {
          id: "1962826825770",
          description:
            "The latest member of the over-the-knee tights family gets an elegant update thanks to a simple line finish. The delicate detail is just as intricate as it is subtle, offering an ideal accessory to amp up your looks. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset30/60 DenierColour: black",
          title: "Over-the-Knee Line Tights",
          handle: "collant-cuissarde-ligne",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_line_560x.jpg?v=1571344043",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_line_720x.jpg?v=1571344043",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_line_966x.jpg?v=1571344043",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_line_1200x.jpg?v=1571344043",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_line_small.jpg?v=1571344043"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_560x.jpg?v=1571344043",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_720x.jpg?v=1571344043",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_966x.jpg?v=1571344043",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_1200x.jpg?v=1571344043",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/over_the_knee_tights_small.jpg?v=1571344043"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_ab47679e-83d4-4a65-a5a9-e3bbc4983adf_560x.jpg?v=1571343831",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_ab47679e-83d4-4a65-a5a9-e3bbc4983adf_720x.jpg?v=1571343831",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_ab47679e-83d4-4a65-a5a9-e3bbc4983adf_966x.jpg?v=1571343831",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_ab47679e-83d4-4a65-a5a9-e3bbc4983adf_1200x.jpg?v=1571343831",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/1_ab47679e-83d4-4a65-a5a9-e3bbc4983adf_small.jpg?v=1571343831"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_7903283c-80e2-4f97-b397-3cd7e258cfdf_560x.jpg?v=1571343831",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_7903283c-80e2-4f97-b397-3cd7e258cfdf_720x.jpg?v=1571343831",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_7903283c-80e2-4f97-b397-3cd7e258cfdf_966x.jpg?v=1571343831",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_7903283c-80e2-4f97-b397-3cd7e258cfdf_1200x.jpg?v=1571343831",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_7903283c-80e2-4f97-b397-3cd7e258cfdf_small.jpg?v=1571343831"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_00399019-d0b8-40ad-bb56-a3c6582bae11_560x.jpg?v=1571343831",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_00399019-d0b8-40ad-bb56-a3c6582bae11_720x.jpg?v=1571343831",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_00399019-d0b8-40ad-bb56-a3c6582bae11_966x.jpg?v=1571343831",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_00399019-d0b8-40ad-bb56-a3c6582bae11_1200x.jpg?v=1571343831",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_00399019-d0b8-40ad-bb56-a3c6582bae11_small.jpg?v=1571343831"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 9
        },

        {
          id: "4258727690282",
          description:
            "Protect your favourite tights and other delicates with this cute and soft cotton storage bag. Ideal to properly store and carry your tights (in your handbag) when travelling.19x23 cmTop with string closure",
          title: "Tights Storage Pouch",
          handle: "rachel-tights-storage-pouch",
          available: true,
          price: 600,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch3_560x.jpg?v=1571429743",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch3_720x.jpg?v=1571429743",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch3_966x.jpg?v=1571429743",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch3_1200x.jpg?v=1571429743",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch3_small.jpg?v=1571429743"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch_560x.jpg?v=1571429743",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch_720x.jpg?v=1571429743",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch_966x.jpg?v=1571429743",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch_1200x.jpg?v=1571429743",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch_small.jpg?v=1571429743"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch2_560x.jpg?v=1571429743",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch2_720x.jpg?v=1571429743",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch2_966x.jpg?v=1571429743",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch2_1200x.jpg?v=1571429743",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Tights_Storage_Pouch2_small.jpg?v=1571429743"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1962869194794",
          description:
            "These graphic tights have a large all-over diamond print for an eye-catching finish. Slip into them for an evening on the town where you won’t go unnoticed. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty Flat seam With cotton gusset Micro-fishnet finishColour: black ",
          title: "Diamond Tights",
          handle: "collant-losange",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights_560x.jpg?v=1571843432",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights_720x.jpg?v=1571843432",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights_966x.jpg?v=1571843432",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights_1200x.jpg?v=1571843432",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights_small.jpg?v=1571843432"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights2_560x.jpg?v=1571843659",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights2_720x.jpg?v=1571843659",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights2_966x.jpg?v=1571843659",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights2_1200x.jpg?v=1571843659",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Diamond_Tights2_small.jpg?v=1571843659"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_fcd14268-8146-4fea-89bf-5e68ee3d1b78_560x.jpg?v=1571843659",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_fcd14268-8146-4fea-89bf-5e68ee3d1b78_720x.jpg?v=1571843659",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_fcd14268-8146-4fea-89bf-5e68ee3d1b78_966x.jpg?v=1571843659",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_fcd14268-8146-4fea-89bf-5e68ee3d1b78_1200x.jpg?v=1571843659",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_fcd14268-8146-4fea-89bf-5e68ee3d1b78_small.jpg?v=1571843659"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_dd79b591-6874-4308-abce-3ab69f212522_560x.jpg?v=1571843659",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_dd79b591-6874-4308-abce-3ab69f212522_720x.jpg?v=1571843659",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_dd79b591-6874-4308-abce-3ab69f212522_966x.jpg?v=1571843659",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_dd79b591-6874-4308-abce-3ab69f212522_1200x.jpg?v=1571843659",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_dd79b591-6874-4308-abce-3ab69f212522_small.jpg?v=1571843659"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_58382a06-3868-4cf1-9d77-7a25d96e2723_560x.jpg?v=1571843659",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_58382a06-3868-4cf1-9d77-7a25d96e2723_720x.jpg?v=1571843659",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_58382a06-3868-4cf1-9d77-7a25d96e2723_966x.jpg?v=1571843659",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_58382a06-3868-4cf1-9d77-7a25d96e2723_1200x.jpg?v=1571843659",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_58382a06-3868-4cf1-9d77-7a25d96e2723_small.jpg?v=1571843659"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 4
        },

        {
          id: "1963631345706",
          description:
            "Feel good about wearing tights with our polka dot tights made from 99% recycled materials. The eco nylon is obtained exclusively from environmentally friendly raw materials, which provides a look that is just as fashionable and resistant as regular nylon! Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset20 DeniersColour: black",
          title: "Eco Polka Dot Tights",
          handle: "pois-recycle",
          available: true,
          price: 1700,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_560x.png?v=1567622297",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_720x.png?v=1567622297",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_966x.png?v=1567622297",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_1200x.png?v=1567622297",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_small.png?v=1567622297"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_14_560x.png?v=1567622297",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_14_720x.png?v=1567622297",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_14_966x.png?v=1567622297",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_14_1200x.png?v=1567622297",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_14_small.png?v=1567622297"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1b2ae571-c59a-4e92-847c-8e162d318137_560x.jpg?v=1567622297",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1b2ae571-c59a-4e92-847c-8e162d318137_720x.jpg?v=1567622297",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1b2ae571-c59a-4e92-847c-8e162d318137_966x.jpg?v=1567622297",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1b2ae571-c59a-4e92-847c-8e162d318137_1200x.jpg?v=1567622297",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1b2ae571-c59a-4e92-847c-8e162d318137_small.jpg?v=1567622297"
            }
          ],
          type: "Tights",
          average_rating: 4.0,
          reviews_count: 1
        },

        {
          id: "1963631509546",
          description:
            "Feel good about wearing tights with our over-the-knee tights made from 99% recycled materials. The eco nylon is obtained exclusively from environmentally friendly raw materials, which provides a look that is just as fashionable and resistant as regular nylon! Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset30/60 DenierColour: black",
          title: "Eco Over-the-Knee Tights",
          handle: "otk-recycle",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_560x.png?v=1567622940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_720x.png?v=1567622940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_966x.png?v=1567622940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_1200x.png?v=1567622940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_small.png?v=1567622940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_560x.png?v=1567622940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_720x.png?v=1567622940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_966x.png?v=1567622940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_1200x.png?v=1567622940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/44_small.png?v=1567622940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/45_560x.png?v=1567622940",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/45_720x.png?v=1567622940",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/45_966x.png?v=1567622940",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/45_1200x.png?v=1567622940",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/45_small.png?v=1567622940"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_560x.jpg?v=1567622930",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_720x.jpg?v=1567622930",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_966x.jpg?v=1567622930",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_1200x.jpg?v=1567622930",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/ecotights_small.jpg?v=1567622930"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "3955256066090",
          description:
            "Sustainability was at the heart of our Fall 2019 Collection with the first release from our eco tights collection featuring our classic Polka Dot Tights and bestselling Over-the-Knee Tights in an eco nylon obtained exclusively from environmentally friendly raw materials. With this pack, you’ll get our 2 exclusive eco tights and our recycled hair elastics at our best price!Pack of 31 pair of Eco Over-the-Knee Tights 1 pair of Eco Polka Dot Tights Pack of 5 hair elastics",
          title: "Eco Pack (3 Piece)",
          handle: "eco-package-3-piece",
          available: true,
          price: 3200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_16_560x.png?v=1567701858",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_16_720x.png?v=1567701858",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_16_966x.png?v=1567701858",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_16_1200x.png?v=1567701858",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_16_small.png?v=1567701858"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x_2x_12e478c3-a232-47bc-a702-8c0faeab9949_560x.jpg?v=1567784111",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x_2x_12e478c3-a232-47bc-a702-8c0faeab9949_720x.jpg?v=1567784111",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x_2x_12e478c3-a232-47bc-a702-8c0faeab9949_966x.jpg?v=1567784111",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x_2x_12e478c3-a232-47bc-a702-8c0faeab9949_1200x.jpg?v=1567784111",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x_2x_12e478c3-a232-47bc-a702-8c0faeab9949_small.jpg?v=1567784111"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_4d264254-dc56-4b5f-a2e1-a6f01797476c_560x.png?v=1567784111",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_4d264254-dc56-4b5f-a2e1-a6f01797476c_720x.png?v=1567784111",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_4d264254-dc56-4b5f-a2e1-a6f01797476c_966x.png?v=1567784111",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_4d264254-dc56-4b5f-a2e1-a6f01797476c_1200x.png?v=1567784111",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_13_4d264254-dc56-4b5f-a2e1-a6f01797476c_small.png?v=1567784111"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_62c5970e-f1fb-4154-bd2f-e18839eb5bab_560x.png?v=1567784111",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_62c5970e-f1fb-4154-bd2f-e18839eb5bab_720x.png?v=1567784111",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_62c5970e-f1fb-4154-bd2f-e18839eb5bab_966x.png?v=1567784111",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_62c5970e-f1fb-4154-bd2f-e18839eb5bab_1200x.png?v=1567784111",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/43_62c5970e-f1fb-4154-bd2f-e18839eb5bab_small.png?v=1567784111"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x_2x_d7d8f8b0-ae26-4435-97d4-4605097538e8_560x.jpg?v=1567784111",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x_2x_d7d8f8b0-ae26-4435-97d4-4605097538e8_720x.jpg?v=1567784111",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x_2x_d7d8f8b0-ae26-4435-97d4-4605097538e8_966x.jpg?v=1567784111",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x_2x_d7d8f8b0-ae26-4435-97d4-4605097538e8_1200x.jpg?v=1567784111",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x_2x_d7d8f8b0-ae26-4435-97d4-4605097538e8_small.jpg?v=1567784111"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x_2x_c8739f1d-b517-461c-8de7-9ea31c5e5541_560x.jpg?v=1567784134",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x_2x_c8739f1d-b517-461c-8de7-9ea31c5e5541_720x.jpg?v=1567784134",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x_2x_c8739f1d-b517-461c-8de7-9ea31c5e5541_966x.jpg?v=1567784134",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x_2x_c8739f1d-b517-461c-8de7-9ea31c5e5541_1200x.jpg?v=1567784134",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x_2x_c8739f1d-b517-461c-8de7-9ea31c5e5541_small.jpg?v=1567784134"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_15_560x.png?v=1567784143",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_15_720x.png?v=1567784143",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_15_966x.png?v=1567784143",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_15_1200x.png?v=1567784143",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_Automne_2019_15_small.png?v=1567784143"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "4258729787434",
          description:
            "Protect your tights and other delicates with this mesh laundry bag. Ideal to properly maintain your tights and save you time.Soft mesh wash bag30x40cm",
          title: "Mesh Leaf Print Wash Bag",
          handle: "mesh-leaf-print-wash-bag",
          available: true,
          price: 600,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sac_de_lavage2_560x.jpg?v=1571929572",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sac_de_lavage2_720x.jpg?v=1571929572",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sac_de_lavage2_966x.jpg?v=1571929572",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sac_de_lavage2_1200x.jpg?v=1571929572",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sac_de_lavage2_small.jpg?v=1571929572"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1962859986986",
          description:
            "Update your essential sheer tights with a subtle back print and add a flirty touch to all your skirts and dresses. The triple back heart print is ideal to complete any look. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seam With cotton gusset30 DenierColour: black",
          title: "Tights with Triple Back Heart Print",
          handle: "collant-trois-coeurs",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_832572eb-2b69-4bdc-8efc-11def985b99f_560x.jpg?v=1565969268",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_832572eb-2b69-4bdc-8efc-11def985b99f_720x.jpg?v=1565969268",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_832572eb-2b69-4bdc-8efc-11def985b99f_966x.jpg?v=1565969268",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_832572eb-2b69-4bdc-8efc-11def985b99f_1200x.jpg?v=1565969268",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_832572eb-2b69-4bdc-8efc-11def985b99f_small.jpg?v=1565969268"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_d4a30be0-2351-45b3-93d5-23e8ab114a23_560x.jpg?v=1565969268",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_d4a30be0-2351-45b3-93d5-23e8ab114a23_720x.jpg?v=1565969268",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_d4a30be0-2351-45b3-93d5-23e8ab114a23_966x.jpg?v=1565969268",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_d4a30be0-2351-45b3-93d5-23e8ab114a23_1200x.jpg?v=1565969268",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_d4a30be0-2351-45b3-93d5-23e8ab114a23_small.jpg?v=1565969268"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 2
        },

        {
          id: "1962831052842",
          description:
            "Polka dots meet star prints in this original tights design! A fun style that’s both trendy and unique, as well as guaranteed to get your legs noticed.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXMicro-fishnet finishPantyFlat seamWith cotton gussetColour: black",
          title: "Polka Dot Star Tights",
          handle: "collants-etoiles-et-pois",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_36f286a5-38fa-4a75-a919-0318f887455b_560x.jpg?v=1565969219",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_36f286a5-38fa-4a75-a919-0318f887455b_720x.jpg?v=1565969219",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_36f286a5-38fa-4a75-a919-0318f887455b_966x.jpg?v=1565969219",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_36f286a5-38fa-4a75-a919-0318f887455b_1200x.jpg?v=1565969219",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_36f286a5-38fa-4a75-a919-0318f887455b_small.jpg?v=1565969219"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_893c8c3d-28fe-4ef6-be0d-32d1f02c9c87_560x.jpg?v=1565969219",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_893c8c3d-28fe-4ef6-be0d-32d1f02c9c87_720x.jpg?v=1565969219",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_893c8c3d-28fe-4ef6-be0d-32d1f02c9c87_966x.jpg?v=1565969219",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_893c8c3d-28fe-4ef6-be0d-32d1f02c9c87_1200x.jpg?v=1565969219",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_893c8c3d-28fe-4ef6-be0d-32d1f02c9c87_small.jpg?v=1565969219"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_3b60b2de-755e-4b2c-9baf-9350ab8d57d5_560x.jpg?v=1565969219",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_3b60b2de-755e-4b2c-9baf-9350ab8d57d5_720x.jpg?v=1565969219",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_3b60b2de-755e-4b2c-9baf-9350ab8d57d5_966x.jpg?v=1565969219",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_3b60b2de-755e-4b2c-9baf-9350ab8d57d5_1200x.jpg?v=1565969219",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_3b60b2de-755e-4b2c-9baf-9350ab8d57d5_small.jpg?v=1565969219"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_db525b5c-d7cb-46c9-86fc-9ca5e6f5adfb_560x.jpg?v=1565969219",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_db525b5c-d7cb-46c9-86fc-9ca5e6f5adfb_720x.jpg?v=1565969219",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_db525b5c-d7cb-46c9-86fc-9ca5e6f5adfb_966x.jpg?v=1565969219",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_db525b5c-d7cb-46c9-86fc-9ca5e6f5adfb_1200x.jpg?v=1565969219",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_db525b5c-d7cb-46c9-86fc-9ca5e6f5adfb_small.jpg?v=1565969219"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "1963688624170",
          description:
            "These over-the-knee-tights truly mimic a high sock with their “sock effect” finish. A best-selling style that was reinvented for fall. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset30/60 DenierColour: black",
          title: "Over-the-Knee Tights with Sock Effect",
          handle: "otk-effet-socks",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_ef6aa2a4-fdcc-4da9-a78b-d028751ddf51_560x.jpg?v=1565732642",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_ef6aa2a4-fdcc-4da9-a78b-d028751ddf51_720x.jpg?v=1565732642",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_ef6aa2a4-fdcc-4da9-a78b-d028751ddf51_966x.jpg?v=1565732642",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_ef6aa2a4-fdcc-4da9-a78b-d028751ddf51_1200x.jpg?v=1565732642",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_ef6aa2a4-fdcc-4da9-a78b-d028751ddf51_small.jpg?v=1565732642"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_48d6b03e-65f8-4234-b444-239be9234036_560x.jpg?v=1565732646",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_48d6b03e-65f8-4234-b444-239be9234036_720x.jpg?v=1565732646",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_48d6b03e-65f8-4234-b444-239be9234036_966x.jpg?v=1565732646",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_48d6b03e-65f8-4234-b444-239be9234036_1200x.jpg?v=1565732646",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_48d6b03e-65f8-4234-b444-239be9234036_small.jpg?v=1565732646"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_59644ca2-2ae3-4961-bf76-94fa92de60ef_560x.jpg?v=1565732646",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_59644ca2-2ae3-4961-bf76-94fa92de60ef_720x.jpg?v=1565732646",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_59644ca2-2ae3-4961-bf76-94fa92de60ef_966x.jpg?v=1565732646",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_59644ca2-2ae3-4961-bf76-94fa92de60ef_1200x.jpg?v=1565732646",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_59644ca2-2ae3-4961-bf76-94fa92de60ef_small.jpg?v=1565732646"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 2
        },

        {
          id: "1962863198250",
          description:
            "With their classic print, Semi-Sheer Chevron Tights are a wardrobe must-have. Add them to any outfit for a touch of originality.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXWith cotton gusset 30 DenierColour: Mustard",
          title: "Mustard Semi-Sheer Chevron Tights",
          handle: "collant-chevrons-moutarde",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_a319f0be-cbbc-416a-8c18-2259749c21dd_560x.jpg?v=1565974153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_a319f0be-cbbc-416a-8c18-2259749c21dd_720x.jpg?v=1565974153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_a319f0be-cbbc-416a-8c18-2259749c21dd_966x.jpg?v=1565974153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_a319f0be-cbbc-416a-8c18-2259749c21dd_1200x.jpg?v=1565974153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_a319f0be-cbbc-416a-8c18-2259749c21dd_small.jpg?v=1565974153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_a91ccdfe-9023-449f-8720-f7eb8e1b9a3d_560x.jpg?v=1565974153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_a91ccdfe-9023-449f-8720-f7eb8e1b9a3d_720x.jpg?v=1565974153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_a91ccdfe-9023-449f-8720-f7eb8e1b9a3d_966x.jpg?v=1565974153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_a91ccdfe-9023-449f-8720-f7eb8e1b9a3d_1200x.jpg?v=1565974153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_1_a91ccdfe-9023-449f-8720-f7eb8e1b9a3d_small.jpg?v=1565974153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouleur_560x.jpg?v=1565974153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouleur_720x.jpg?v=1565974153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouleur_966x.jpg?v=1565974153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouleur_1200x.jpg?v=1565974153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouleur_small.jpg?v=1565974153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_dea757ff-8891-41dc-8c44-0e4335eef855_560x.jpg?v=1565974153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_dea757ff-8891-41dc-8c44-0e4335eef855_720x.jpg?v=1565974153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_dea757ff-8891-41dc-8c44-0e4335eef855_966x.jpg?v=1565974153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_dea757ff-8891-41dc-8c44-0e4335eef855_1200x.jpg?v=1565974153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_dea757ff-8891-41dc-8c44-0e4335eef855_small.jpg?v=1565974153"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1962865033258",
          description:
            "Move over polka dots, it’s time to make room for these Vertical Line Tights. A simple tights style that is equally original, they will quickly become a wardrobe staple. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seam With cotton gusset30 DenierColour: black ",
          title: "Vertical Line Tights",
          handle: "collant-verticaux",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_13df3967-baeb-4667-b09e-8c964d79618c_560x.jpg?v=1565730700",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_13df3967-baeb-4667-b09e-8c964d79618c_720x.jpg?v=1565730700",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_13df3967-baeb-4667-b09e-8c964d79618c_966x.jpg?v=1565730700",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_13df3967-baeb-4667-b09e-8c964d79618c_1200x.jpg?v=1565730700",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_13df3967-baeb-4667-b09e-8c964d79618c_small.jpg?v=1565730700"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2d80dbb4-8e5d-4150-bb6b-733dd2170de1_560x.jpg?v=1565730709",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2d80dbb4-8e5d-4150-bb6b-733dd2170de1_720x.jpg?v=1565730709",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2d80dbb4-8e5d-4150-bb6b-733dd2170de1_966x.jpg?v=1565730709",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2d80dbb4-8e5d-4150-bb6b-733dd2170de1_1200x.jpg?v=1565730709",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_2d80dbb4-8e5d-4150-bb6b-733dd2170de1_small.jpg?v=1565730709"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_2dc33cdf-6d97-4254-a290-825b975b3bf8_560x.jpg?v=1565730709",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_2dc33cdf-6d97-4254-a290-825b975b3bf8_720x.jpg?v=1565730709",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_2dc33cdf-6d97-4254-a290-825b975b3bf8_966x.jpg?v=1565730709",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_2dc33cdf-6d97-4254-a290-825b975b3bf8_1200x.jpg?v=1565730709",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_2dc33cdf-6d97-4254-a290-825b975b3bf8_small.jpg?v=1565730709"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "1962867294250",
          description:
            "The newest way to wear polka dots is with Rachel’s Polka Dot Back Seam Tights! These tights are all business in the front with a style twist in the back. Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPanty30 DenierColour: black",
          title: "Polka Dot Back Seam Tights",
          handle: "collant-tiret",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_42e3fd0f-43fd-492a-b462-7fa4d5e03c9e_560x.jpg?v=1565731141",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_42e3fd0f-43fd-492a-b462-7fa4d5e03c9e_720x.jpg?v=1565731141",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_42e3fd0f-43fd-492a-b462-7fa4d5e03c9e_966x.jpg?v=1565731141",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_42e3fd0f-43fd-492a-b462-7fa4d5e03c9e_1200x.jpg?v=1565731141",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_42e3fd0f-43fd-492a-b462-7fa4d5e03c9e_small.jpg?v=1565731141"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_29865409-3422-4c2b-a30a-46f8f86d9938_560x.jpg?v=1565731152",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_29865409-3422-4c2b-a30a-46f8f86d9938_720x.jpg?v=1565731152",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_29865409-3422-4c2b-a30a-46f8f86d9938_966x.jpg?v=1565731152",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_29865409-3422-4c2b-a30a-46f8f86d9938_1200x.jpg?v=1565731152",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_29865409-3422-4c2b-a30a-46f8f86d9938_small.jpg?v=1565731152"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_3565aa8e-4967-47ec-ab5a-f4e57ad5c458_560x.jpg?v=1565731152",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_3565aa8e-4967-47ec-ab5a-f4e57ad5c458_720x.jpg?v=1565731152",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_3565aa8e-4967-47ec-ab5a-f4e57ad5c458_966x.jpg?v=1565731152",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_3565aa8e-4967-47ec-ab5a-f4e57ad5c458_1200x.jpg?v=1565731152",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_3565aa8e-4967-47ec-ab5a-f4e57ad5c458_small.jpg?v=1565731152"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1f718258-2ac9-4296-bf1d-33c6b765a0ab_560x.jpg?v=1565731152",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1f718258-2ac9-4296-bf1d-33c6b765a0ab_720x.jpg?v=1565731152",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1f718258-2ac9-4296-bf1d-33c6b765a0ab_966x.jpg?v=1565731152",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1f718258-2ac9-4296-bf1d-33c6b765a0ab_1200x.jpg?v=1565731152",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1f718258-2ac9-4296-bf1d-33c6b765a0ab_small.jpg?v=1565731152"
            }
          ],
          type: "Tights",
          average_rating: 4.0,
          reviews_count: 4
        },

        {
          id: "1962864836650",
          description:
            "With their classic print, Semi-Sheer Chevron Tights are a wardrobe must-have. Add them to any outfit for a touch of originality.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXWith cotton gusset 30 DenierColour: Burgundy",
          title: "Burgundy Semi-Sheer Chevron Tights",
          handle: "collant-chevrons-bordeaux",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_aa7b32d8-8571-4905-852b-512c6c24b686_560x.jpg?v=1565731832",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_aa7b32d8-8571-4905-852b-512c6c24b686_720x.jpg?v=1565731832",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_aa7b32d8-8571-4905-852b-512c6c24b686_966x.jpg?v=1565731832",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_aa7b32d8-8571-4905-852b-512c6c24b686_1200x.jpg?v=1565731832",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_aa7b32d8-8571-4905-852b-512c6c24b686_small.jpg?v=1565731832"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_6e7e8636-9b39-4c35-b950-6dabb3f9d425_560x.jpg?v=1565731835",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_6e7e8636-9b39-4c35-b950-6dabb3f9d425_720x.jpg?v=1565731835",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_6e7e8636-9b39-4c35-b950-6dabb3f9d425_966x.jpg?v=1565731835",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_6e7e8636-9b39-4c35-b950-6dabb3f9d425_1200x.jpg?v=1565731835",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_6e7e8636-9b39-4c35-b950-6dabb3f9d425_small.jpg?v=1565731835"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1962860838954",
          description:
            "With their classic print, Semi-Sheer Chevron Tights are a wardrobe must-have. Add them to any outfit for a touch of originality.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXWith cotton gusset 30 DenierColour: Navy",
          title: "Navy Semi-Sheer Chevron Tights",
          handle: "collant-chevrons-bleu-marine",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_2_560x.jpg?v=1565731572",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_2_720x.jpg?v=1565731572",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_2_966x.jpg?v=1565731572",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_2_1200x.jpg?v=1565731572",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_2_small.jpg?v=1565731572"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_6281555d-46bb-4db8-aca6-111a89372d24_560x.jpg?v=1565731577",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_6281555d-46bb-4db8-aca6-111a89372d24_720x.jpg?v=1565731577",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_6281555d-46bb-4db8-aca6-111a89372d24_966x.jpg?v=1565731577",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_6281555d-46bb-4db8-aca6-111a89372d24_1200x.jpg?v=1565731577",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_6281555d-46bb-4db8-aca6-111a89372d24_small.jpg?v=1565731577"
            }
          ],
          type: "Tights",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1905351721002",
          description:
            "Made for the polka dot lovers, these tights feature an original 5-dot print that will easily complement just about any outfit.Made in Italy Comfortable and resistantPantyIntegrated soleFlat seam With cotton gusset30 DeniersColour : Black",
          title: "Tights with 5-Dot Print",
          handle: "tights-with-five-dot-print",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a0846726-5556-4441-b31e-4d541c12df6b_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a0846726-5556-4441-b31e-4d541c12df6b_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a0846726-5556-4441-b31e-4d541c12df6b_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a0846726-5556-4441-b31e-4d541c12df6b_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a0846726-5556-4441-b31e-4d541c12df6b_small.jpg?v=1562878910"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_f8a4f5ca-72e9-4cba-97cd-caf13f582f4d_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_f8a4f5ca-72e9-4cba-97cd-caf13f582f4d_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_f8a4f5ca-72e9-4cba-97cd-caf13f582f4d_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_f8a4f5ca-72e9-4cba-97cd-caf13f582f4d_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_f8a4f5ca-72e9-4cba-97cd-caf13f582f4d_small.jpg?v=1562878910"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_5dots_small.jpg?v=1562878910"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_2962c47e-40f3-439e-af59-b2a56f3220c1_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_2962c47e-40f3-439e-af59-b2a56f3220c1_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_2962c47e-40f3-439e-af59-b2a56f3220c1_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_2962c47e-40f3-439e-af59-b2a56f3220c1_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_2962c47e-40f3-439e-af59-b2a56f3220c1_small.jpg?v=1562878910"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dottights_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dottights_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dottights_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dottights_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dottights_small.jpg?v=1562878910"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmotifs_560x.jpg?v=1562878910",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmotifs_720x.jpg?v=1562878910",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmotifs_966x.jpg?v=1562878910",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmotifs_1200x.jpg?v=1562878910",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmotifs_small.jpg?v=1562878910"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 9
        },

        {
          id: "1905352310826",
          description:
            "Make a statement this season in these grey tights featuring an all-over pink polka dot print. Their original finish won’t go unnoticed. Made in Italy Comfortable and resistantFlat seam With cotton gusset40 DeniersColour : Grey",
          title: "Grey Pink Polka Dot Tights",
          handle: "grey-pink-polka-dot-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrispois_560x.jpg?v=1562878865",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrispois_720x.jpg?v=1562878865",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrispois_966x.jpg?v=1562878865",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrispois_1200x.jpg?v=1562878865",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrispois_small.jpg?v=1562878865"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgris_560x.jpg?v=1562878865",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgris_720x.jpg?v=1562878865",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgris_966x.jpg?v=1562878865",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgris_1200x.jpg?v=1562878865",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgris_small.jpg?v=1562878865"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 24
        },

        {
          id: "1855380291626",
          description:
            "Add to cart these innovative hair elastics made from recycled tights and be ready for any type of hair day.",
          title: "Recycled Hair Elastics (5)",
          handle: "elastiques-cheveux",
          available: true,
          price: 300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_560x.jpg?v=1550613010",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_720x.jpg?v=1550613010",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_966x.jpg?v=1550613010",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_1200x.jpg?v=1550613010",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics1_small.jpg?v=1550613010"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_560x.jpg?v=1550613010",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_720x.jpg?v=1550613010",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_966x.jpg?v=1550613010",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_1200x.jpg?v=1550613010",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics3_small.jpg?v=1550613010"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_560x.jpg?v=1550613010",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_720x.jpg?v=1550613010",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_966x.jpg?v=1550613010",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_1200x.jpg?v=1550613010",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/elastique3.1_small.jpg?v=1550613010"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics2_560x.jpg?v=1550613010",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics2_720x.jpg?v=1550613010",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics2_966x.jpg?v=1550613010",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics2_1200x.jpg?v=1550613010",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Elastics2_small.jpg?v=1550613010"
            }
          ],
          type: "Default",
          average_rating: 4.5,
          reviews_count: 4
        },

        {
          id: "1591928848426",
          description:
            "If you’re crazy for polka dots, you’ll love Micro Dot Tights! A sheer tight with an all-over polka dot pattern that will provide just the edge you’ve been looking for to all your looks. Made in Italy Comfortable and resistantPanty Flat seam With cotton gusset30 DeniersColor : Black",
          title: "Micro Dot Tights",
          handle: "micro-dots-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot2_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot2_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot2_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot2_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot2_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmicropois_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmicropois_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmicropois_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmicropois_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmicropois_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_e6d31a39-6140-44a6-b7b2-5aa2295963dc_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_e6d31a39-6140-44a6-b7b2-5aa2295963dc_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_e6d31a39-6140-44a6-b7b2-5aa2295963dc_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_e6d31a39-6140-44a6-b7b2-5aa2295963dc_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_e6d31a39-6140-44a6-b7b2-5aa2295963dc_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot3_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot3_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot3_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot3_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot3_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot5_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot5_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot5_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot5_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot5_small.jpg?v=1552595510"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot4_560x.jpg?v=1552595510",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot4_720x.jpg?v=1552595510",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot4_966x.jpg?v=1552595510",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot4_1200x.jpg?v=1552595510",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/microdot4_small.jpg?v=1552595510"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 47
        },

        {
          id: "1365689237546",
          description:
            "Your favourite Over-the-Knee Tights get a new updated look with a scallop finish. The feminine detailing is perfect to dress up any basic look.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset30 / 60 DeniersColor : black~Let these tights take center stage by pairing them with a simple fitted dress for a day time look that will definitely get you noticed.",
          title: "Scalloped Over-the-Knee Tights",
          handle: "scalloped-over-the-knee-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwave_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwave_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwave_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwave_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwave_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvagues_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvagues_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvagues_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvagues_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvagues_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_47dd22ba-29e6-4458-a448-f94dfcefaacf_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_47dd22ba-29e6-4458-a448-f94dfcefaacf_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_47dd22ba-29e6-4458-a448-f94dfcefaacf_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_47dd22ba-29e6-4458-a448-f94dfcefaacf_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_47dd22ba-29e6-4458-a448-f94dfcefaacf_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague_small.jpg?v=1552595690"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague2_d67bdf04-6a1a-4a68-94f9-4e5ae4b09b34_560x.jpg?v=1552595690",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague2_d67bdf04-6a1a-4a68-94f9-4e5ae4b09b34_720x.jpg?v=1552595690",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague2_d67bdf04-6a1a-4a68-94f9-4e5ae4b09b34_966x.jpg?v=1552595690",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague2_d67bdf04-6a1a-4a68-94f9-4e5ae4b09b34_1200x.jpg?v=1552595690",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkvague2_d67bdf04-6a1a-4a68-94f9-4e5ae4b09b34_small.jpg?v=1552595690"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 181
        },

        {
          id: "1365630025770",
          description:
            "These sheer tights have an elegant floral design and polka dot detailing for an eye-catching finish that can complete any day or evening look.Made in ItalyBoxer brief  No gussetAutomatic seams30 /60 Deniers Color : black",
          title: "Sheer Floral Polka Dot Tights",
          handle: "sheer-floral-polka-dot-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleurspois_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleurspois_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleurspois_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleurspois_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleurspois_small.jpg?v=1552595409"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_small.jpg?v=1552595409"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral-768x1024_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral-768x1024_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral-768x1024_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral-768x1024_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral-768x1024_small.jpg?v=1552595409"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_0f283a12-6638-44f6-8166-7f97b192ea30_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_0f283a12-6638-44f6-8166-7f97b192ea30_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_0f283a12-6638-44f6-8166-7f97b192ea30_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_0f283a12-6638-44f6-8166-7f97b192ea30_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleur_0f283a12-6638-44f6-8166-7f97b192ea30_small.jpg?v=1552595409"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfloral_small.jpg?v=1552595409"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleuri_560x.jpg?v=1552595409",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleuri_720x.jpg?v=1552595409",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleuri_966x.jpg?v=1552595409",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleuri_1200x.jpg?v=1552595409",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantfleuri_small.jpg?v=1552595409"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 182
        },

        {
          id: "1905351065642",
          description:
            "Over-the-knee tights have been reimagined with zigzag detailing this season. Located above the knee, the zigzag print provides the perfect style twist.Made in Italy Boxer brief  With cotton gusset Automatic seams30/60 Deniers Colour : black",
          title: "Over-the-Knee Zigzag Tights",
          handle: "over-the-knee-zigzag-tights",
          available: true,
          price: 1300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_aabb3d99-7b5e-411f-9101-9d3f011b071e_560x.jpg?v=1571339463",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_aabb3d99-7b5e-411f-9101-9d3f011b071e_720x.jpg?v=1571339463",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_aabb3d99-7b5e-411f-9101-9d3f011b071e_966x.jpg?v=1571339463",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_aabb3d99-7b5e-411f-9101-9d3f011b071e_1200x.jpg?v=1571339463",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otk_aabb3d99-7b5e-411f-9101-9d3f011b071e_small.jpg?v=1571339463"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_2d8226d1-78c4-4ed4-9887-f2770211a80c_560x.jpg?v=1571339463",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_2d8226d1-78c4-4ed4-9887-f2770211a80c_720x.jpg?v=1571339463",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_2d8226d1-78c4-4ed4-9887-f2770211a80c_966x.jpg?v=1571339463",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_2d8226d1-78c4-4ed4-9887-f2770211a80c_1200x.jpg?v=1571339463",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_2d8226d1-78c4-4ed4-9887-f2770211a80c_small.jpg?v=1571339463"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_59148a50-659c-4e46-ae97-bbe7e9e3f97e_560x.jpg?v=1571339463",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_59148a50-659c-4e46-ae97-bbe7e9e3f97e_720x.jpg?v=1571339463",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_59148a50-659c-4e46-ae97-bbe7e9e3f97e_966x.jpg?v=1571339463",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_59148a50-659c-4e46-ae97-bbe7e9e3f97e_1200x.jpg?v=1571339463",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_59148a50-659c-4e46-ae97-bbe7e9e3f97e_small.jpg?v=1571339463"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_ab79f5f8-707e-49f7-a77d-a54906aa736e_560x.jpg?v=1571339463",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_ab79f5f8-707e-49f7-a77d-a54906aa736e_720x.jpg?v=1571339463",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_ab79f5f8-707e-49f7-a77d-a54906aa736e_966x.jpg?v=1571339463",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_ab79f5f8-707e-49f7-a77d-a54906aa736e_1200x.jpg?v=1571339463",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_ab79f5f8-707e-49f7-a77d-a54906aa736e_small.jpg?v=1571339463"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_9d55e27c-92db-4d68-81de-639ab81139c2_560x.jpg?v=1571339463",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_9d55e27c-92db-4d68-81de-639ab81139c2_720x.jpg?v=1571339463",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_9d55e27c-92db-4d68-81de-639ab81139c2_966x.jpg?v=1571339463",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_9d55e27c-92db-4d68-81de-639ab81139c2_1200x.jpg?v=1571339463",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_9d55e27c-92db-4d68-81de-639ab81139c2_small.jpg?v=1571339463"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 24
        },

        {
          id: "806344327210",
          description:
            "If you like our original over-the-knee tights, you’ll love this polka dot version!Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset30 / 60 DeniersColor : black",
          title: "Over-The-Knee tights with Dots",
          handle: "over-the-knee-tights-with-dots",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardepois_c15d6861-da33-4c1b-a6e4-a0b9490328e0_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardepois_c15d6861-da33-4c1b-a6e4-a0b9490328e0_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardepois_c15d6861-da33-4c1b-a6e4-a0b9490328e0_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardepois_c15d6861-da33-4c1b-a6e4-a0b9490328e0_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardepois_c15d6861-da33-4c1b-a6e4-a0b9490328e0_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardePois_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardePois_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardePois_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardePois_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardePois_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardespois_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardespois_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardespois_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardespois_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardespois_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_11e27f64-77b3-4fe5-8e64-53af489204f6_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_11e27f64-77b3-4fe5-8e64-53af489204f6_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_11e27f64-77b3-4fe5-8e64-53af489204f6_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_11e27f64-77b3-4fe5-8e64-53af489204f6_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkpois_11e27f64-77b3-4fe5-8e64-53af489204f6_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois_small.jpg?v=1552595290"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois2_560x.jpg?v=1552595290",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois2_720x.jpg?v=1552595290",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois2_966x.jpg?v=1552595290",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois2_1200x.jpg?v=1552595290",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes-pois2_small.jpg?v=1552595290"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 135
        },

        {
          id: "1341102719018",
          description:
            "This is our best selling tights style! A pair of over-the-knee tights is perfect to put a trendy urban touch to your outfit !Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXPantyFlat seamWith cotton gusset30 / 60 DeniersColor : black",
          title: "Over-The-Knee Tights",
          handle: "over-the-knee-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_cd86d71a-81e7-4258-b0f9-cf4d897d00f1_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_cd86d71a-81e7-4258-b0f9-cf4d897d00f1_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_cd86d71a-81e7-4258-b0f9-cf4d897d00f1_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_cd86d71a-81e7-4258-b0f9-cf4d897d00f1_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_cd86d71a-81e7-4258-b0f9-cf4d897d00f1_small.jpg?v=1552595785"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissarde_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissarde_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissarde_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissarde_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissarde_small.jpg?v=1552595785"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes2_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes2_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes2_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes2_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes2_small.jpg?v=1552595785"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights1_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights1_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights1_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights1_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights1_small.jpg?v=1552595785"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-cuissardes_small.jpg?v=1552595785"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_560x.jpg?v=1552595785",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_720x.jpg?v=1552595785",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_966x.jpg?v=1552595785",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_1200x.jpg?v=1552595785",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes1_small.jpg?v=1552595785"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 135
        },

        {
          id: "1905352114218",
          description:
            "With their classic print, Semi-Sheer Chevron Tights are a wardrobe must-have. Add them to any outfit for a touch of originality.Made in Italy Comfortable and resistantHigh-waist panty for figure flattering fit  With cotton gusset30 DeniersColour: Black",
          title: "Semi-Sheer Chevron Tights",
          handle: "semi-sheer-chevron-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron2_560x.jpg?v=1555440519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron2_720x.jpg?v=1555440519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron2_966x.jpg?v=1555440519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron2_1200x.jpg?v=1555440519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron2_small.jpg?v=1555440519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_cf4aec73-a640-4baa-b854-27df3538b5e0_560x.jpg?v=1555440519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_cf4aec73-a640-4baa-b854-27df3538b5e0_720x.jpg?v=1555440519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_cf4aec73-a640-4baa-b854-27df3538b5e0_966x.jpg?v=1555440519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_cf4aec73-a640-4baa-b854-27df3538b5e0_1200x.jpg?v=1555440519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_cf4aec73-a640-4baa-b854-27df3538b5e0_small.jpg?v=1555440519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_560x.jpg?v=1555440519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_720x.jpg?v=1555440519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_966x.jpg?v=1555440519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_1200x.jpg?v=1555440519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_chevronNoir_small.jpg?v=1555440519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevronnoir_d4d1dd93-f31c-42aa-9115-eb0266882fc5_560x.jpg?v=1555440519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevronnoir_d4d1dd93-f31c-42aa-9115-eb0266882fc5_720x.jpg?v=1555440519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevronnoir_d4d1dd93-f31c-42aa-9115-eb0266882fc5_966x.jpg?v=1555440519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevronnoir_d4d1dd93-f31c-42aa-9115-eb0266882fc5_1200x.jpg?v=1555440519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevronnoir_d4d1dd93-f31c-42aa-9115-eb0266882fc5_small.jpg?v=1555440519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron_23c2e288-a0d6-4b78-a9ff-07f35507b5e8_560x.jpg?v=1555440519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron_23c2e288-a0d6-4b78-a9ff-07f35507b5e8_720x.jpg?v=1555440519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron_23c2e288-a0d6-4b78-a9ff-07f35507b5e8_966x.jpg?v=1555440519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron_23c2e288-a0d6-4b78-a9ff-07f35507b5e8_1200x.jpg?v=1555440519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chevron_23c2e288-a0d6-4b78-a9ff-07f35507b5e8_small.jpg?v=1555440519"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 18
        },

        {
          id: "781824065578",
          description:
            "Black Polka Dot Tights will give you a sophisticated look! Easy to wear with your  wardrobe, it can be worn for any occasion.Made in Italy Comfortable and resistantPanty Flat seam With cotton gusset30 DeniersColour : Black",
          title: "Black Polka Dot Tights",
          handle: "black-dot-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-pois_560x.jpg?v=1529606555",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-pois_720x.jpg?v=1529606555",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-pois_966x.jpg?v=1529606555",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-pois_1200x.jpg?v=1529606555",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-pois_small.jpg?v=1529606555"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_560x.jpg?v=1529606555",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_720x.jpg?v=1529606555",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_966x.jpg?v=1529606555",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_1200x.jpg?v=1529606555",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_small.jpg?v=1529606555"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 180
        },

        {
          id: "1460762738730",
          description:
            "Our back seam tights are an ideal accessory to add a touch of femininity to all your looks. ",
          title: "Backseam Tights",
          handle: "backseam-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/backseamtights_560x.jpg?v=1552595560",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/backseamtights_720x.jpg?v=1552595560",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/backseamtights_966x.jpg?v=1552595560",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/backseamtights_1200x.jpg?v=1552595560",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/backseamtights_small.jpg?v=1552595560"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_560x.jpg?v=1552595560",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_720x.jpg?v=1552595560",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_966x.jpg?v=1552595560",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_1200x.jpg?v=1552595560",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_small.jpg?v=1552595560"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_d7baa1ac-a3cf-41cd-a197-ac522a37f357_560x.jpg?v=1552595560",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_d7baa1ac-a3cf-41cd-a197-ac522a37f357_720x.jpg?v=1552595560",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_d7baa1ac-a3cf-41cd-a197-ac522a37f357_966x.jpg?v=1552595560",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_d7baa1ac-a3cf-41cd-a197-ac522a37f357_1200x.jpg?v=1552595560",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_d7baa1ac-a3cf-41cd-a197-ac522a37f357_small.jpg?v=1552595560"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouture_560x.jpg?v=1552595560",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouture_720x.jpg?v=1552595560",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouture_966x.jpg?v=1552595560",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouture_1200x.jpg?v=1552595560",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcouture_small.jpg?v=1552595560"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 57
        },

        {
          id: "1913109807146",
          description:
            "Your favourite Over-the-Knee Tights get a new updated look with a scallop finish. The feminine detailing is perfect to dress up any basic look.These over-the-knee tights are designed with a special waistband that won’t pinch your waist and feature a control panty that will give you a chic fitted look.Made in ItalyComfortable and resistantStandard 100 certified by OEKO-TEXBoxer briefAutomatic seamNo gusset30 / 60 DeniersColour : black",
          title: "Scalloped OTK Tights (Plus Size)",
          handle: "scalloped-over-the-knee-tights-plus-size",
          available: true,
          price: 1300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_78e50b0c-8214-4ea9-884c-b5b34dfaa3f3_560x.jpg?v=1553806405",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_78e50b0c-8214-4ea9-884c-b5b34dfaa3f3_720x.jpg?v=1553806405",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_78e50b0c-8214-4ea9-884c-b5b34dfaa3f3_966x.jpg?v=1553806405",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_78e50b0c-8214-4ea9-884c-b5b34dfaa3f3_1200x.jpg?v=1553806405",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_78e50b0c-8214-4ea9-884c-b5b34dfaa3f3_small.jpg?v=1553806405"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/plussizetights_560x.jpg?v=1553806405",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/plussizetights_720x.jpg?v=1553806405",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/plussizetights_966x.jpg?v=1553806405",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/plussizetights_1200x.jpg?v=1553806405",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/plussizetights_small.jpg?v=1553806405"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_a1e86cef-dd56-4b54-b5cd-e8b4f42c2d0c_560x.jpg?v=1553806405",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_a1e86cef-dd56-4b54-b5cd-e8b4f42c2d0c_720x.jpg?v=1553806405",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_a1e86cef-dd56-4b54-b5cd-e8b4f42c2d0c_966x.jpg?v=1553806405",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_a1e86cef-dd56-4b54-b5cd-e8b4f42c2d0c_1200x.jpg?v=1553806405",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_a1e86cef-dd56-4b54-b5cd-e8b4f42c2d0c_small.jpg?v=1553806405"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkplussize_560x.jpg?v=1553806405",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkplussize_720x.jpg?v=1553806405",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkplussize_966x.jpg?v=1553806405",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkplussize_1200x.jpg?v=1553806405",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkplussize_small.jpg?v=1553806405"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkqueensize_560x.jpg?v=1553806405",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkqueensize_720x.jpg?v=1553806405",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkqueensize_966x.jpg?v=1553806405",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkqueensize_1200x.jpg?v=1553806405",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkqueensize_small.jpg?v=1553806405"
            }
          ],
          type: "Plus Size Tights",
          average_rating: 5.0,
          reviews_count: 6
        },

        {
          id: "1343793135658",
          description:
            "Add a feminine touch to your outfit with these tights featuring a back heart seam detailing. It’s a small detail that will make a big difference.Now available in extended sizes, these back heart seam tights are designed with a special waistband that won’t pinch your waist and feature a control panty that will give you a chic fitted look.",
          title: "Back Heart Seam Tights (Plus Size)",
          handle: "back-heart-seam-tights-plus-size",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeurs_560x.jpg?v=1529606777",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeurs_720x.jpg?v=1529606777",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeurs_966x.jpg?v=1529606777",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeurs_1200x.jpg?v=1529606777",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeurs_small.jpg?v=1529606777"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligneCoeur_8323b922-a716-4c47-81d7-6c708db738ad_560x.jpg?v=1529606777",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligneCoeur_8323b922-a716-4c47-81d7-6c708db738ad_720x.jpg?v=1529606777",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligneCoeur_8323b922-a716-4c47-81d7-6c708db738ad_966x.jpg?v=1529606777",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligneCoeur_8323b922-a716-4c47-81d7-6c708db738ad_1200x.jpg?v=1529606777",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligneCoeur_8323b922-a716-4c47-81d7-6c708db738ad_small.jpg?v=1529606777"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus2_560x.jpg?v=1529606777",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus2_720x.jpg?v=1529606777",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus2_966x.jpg?v=1529606777",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus2_1200x.jpg?v=1529606777",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus2_small.jpg?v=1529606777"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeur2_560x.jpg?v=1529606777",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeur2_720x.jpg?v=1529606777",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeur2_966x.jpg?v=1529606777",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeur2_1200x.jpg?v=1529606777",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscoeur2_small.jpg?v=1529606777"
            }
          ],
          type: "Plus Size Tights",
          average_rating: 5.0,
          reviews_count: 21
        },

        {
          id: "781795426346",
          description:
            "These opaque tights with white polka dot detailing will keep you warm and on trend this season. Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset40 DeniersColor : Black",
          title: "Black Tights with White Polka Dots",
          handle: "black-tights-with-white-polka-dots",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoirpoisblanc_560x.jpg?v=1539115679",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoirpoisblanc_720x.jpg?v=1539115679",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoirpoisblanc_966x.jpg?v=1539115679",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoirpoisblanc_1200x.jpg?v=1539115679",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoirpoisblanc_small.jpg?v=1539115679"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrospois4_560x.jpg?v=1539115679",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrospois4_720x.jpg?v=1539115679",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrospois4_966x.jpg?v=1539115679",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrospois4_1200x.jpg?v=1539115679",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrospois4_small.jpg?v=1539115679"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_560x.jpg?v=1539115679",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_720x.jpg?v=1539115679",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_966x.jpg?v=1539115679",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_1200x.jpg?v=1539115679",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Produits_small.jpg?v=1539115679"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 62
        },

        {
          id: "1591931764778",
          description:
            "Hearts meet opaque tights with this intricate design that’s perfect for the new season.  Made in Italy Comfortable and resistantFlat seam With cotton gusset3D Effect 60 DeniersColor : black",
          title: "Opaque Heart Print Tights",
          handle: "opaque-heart-tights",
          available: false,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeuropaque_560x.jpg?v=1533571657",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeuropaque_720x.jpg?v=1533571657",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeuropaque_966x.jpg?v=1533571657",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeuropaque_1200x.jpg?v=1533571657",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeuropaque_small.jpg?v=1533571657"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantopaquecoeur_560x.jpg?v=1533571675",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantopaquecoeur_720x.jpg?v=1533571675",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantopaquecoeur_966x.jpg?v=1533571675",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantopaquecoeur_1200x.jpg?v=1533571675",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantopaquecoeur_small.jpg?v=1533571675"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_0f857269-1c2b-4798-ac22-3d2563c9dd76_560x.jpg?v=1533571687",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_0f857269-1c2b-4798-ac22-3d2563c9dd76_720x.jpg?v=1533571687",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_0f857269-1c2b-4798-ac22-3d2563c9dd76_966x.jpg?v=1533571687",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_0f857269-1c2b-4798-ac22-3d2563c9dd76_1200x.jpg?v=1533571687",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_0f857269-1c2b-4798-ac22-3d2563c9dd76_small.jpg?v=1533571687"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 46
        },

        {
          id: "1323773526058",
          description:
            "These over-the-knee tights are a closet essential for the colder months. A winter version of our best-selling over-the-knee tights, this accessory will keep you warm and stylish all season long.",
          title: "Warm Over-The-Knee Tights",
          handle: "warm-over-the-knee-tights",
          available: false,
          price: 2000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotktights_560x.jpg?v=1550097310",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotktights_720x.jpg?v=1550097310",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotktights_966x.jpg?v=1550097310",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotktights_1200x.jpg?v=1550097310",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotktights_small.jpg?v=1550097310"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardeschaud-720x960_560x.jpg?v=1550097310",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardeschaud-720x960_720x.jpg?v=1550097310",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardeschaud-720x960_966x.jpg?v=1550097310",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardeschaud-720x960_1200x.jpg?v=1550097310",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardeschaud-720x960_small.jpg?v=1550097310"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/noir_bas_560x.jpg?v=1550097310",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/noir_bas_720x.jpg?v=1550097310",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/noir_bas_966x.jpg?v=1550097310",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/noir_bas_1200x.jpg?v=1550097310",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/noir_bas_small.jpg?v=1550097310"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotk_560x.jpg?v=1550097310",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotk_720x.jpg?v=1550097310",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotk_966x.jpg?v=1550097310",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotk_1200x.jpg?v=1550097310",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmotk_small.jpg?v=1550097310"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeshiver_560x.jpg?v=1550097557",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeshiver_720x.jpg?v=1550097557",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeshiver_966x.jpg?v=1550097557",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeshiver_1200x.jpg?v=1550097557",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeshiver_small.jpg?v=1550097557"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeschaud_560x.jpg?v=1550097559",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeschaud_720x.jpg?v=1550097559",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeschaud_966x.jpg?v=1550097559",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeschaud_1200x.jpg?v=1550097559",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cuissardeschaud_small.jpg?v=1550097559"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 115
        },

        {
          id: "1909918564394",
          description:
            "With their all-over chevron print, these tights are as versatile as they are fun. Add them to your wardrobe for a unique look. Made in Italy Comfortable and resistant90 DeniersColour: Black",
          title: "Textured Black Chevron Tights",
          handle: "textured-black-chevron-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron3_560x.jpg?v=1552429429",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron3_720x.jpg?v=1552429429",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron3_966x.jpg?v=1552429429",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron3_1200x.jpg?v=1552429429",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron3_small.jpg?v=1552429429"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron2_560x.jpg?v=1552429429",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron2_720x.jpg?v=1552429429",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron2_966x.jpg?v=1552429429",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron2_1200x.jpg?v=1552429429",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron2_small.jpg?v=1552429429"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_4802aa16-5031-426f-8718-dd4107d1e517_560x.jpg?v=1552429448",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_4802aa16-5031-426f-8718-dd4107d1e517_720x.jpg?v=1552429448",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_4802aa16-5031-426f-8718-dd4107d1e517_966x.jpg?v=1552429448",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_4802aa16-5031-426f-8718-dd4107d1e517_1200x.jpg?v=1552429448",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchevron_4802aa16-5031-426f-8718-dd4107d1e517_small.jpg?v=1552429448"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 13
        },

        {
          id: "1591932026922",
          description:
            "Burgundy tights are updated with white polka dots this season, offering a new opaque tights style, you’ll love to have in your wardrobe.Made in Italy Comfortable and resistantFlat seam With cotton gusset40 DeniersColor : Burgundy",
          title: "Burgundy Polka Dot Tights",
          handle: "burgundy-tights-with-white-polka-dots",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_22f164eb-4deb-4f7c-ac60-9845bf7d393b_560x.jpg?v=1543432459",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_22f164eb-4deb-4f7c-ac60-9845bf7d393b_720x.jpg?v=1543432459",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_22f164eb-4deb-4f7c-ac60-9845bf7d393b_966x.jpg?v=1543432459",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_22f164eb-4deb-4f7c-ac60-9845bf7d393b_1200x.jpg?v=1543432459",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_22f164eb-4deb-4f7c-ac60-9845bf7d393b_small.jpg?v=1543432459"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbordeaux_560x.jpg?v=1543432459",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbordeaux_720x.jpg?v=1543432459",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbordeaux_966x.jpg?v=1543432459",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbordeaux_1200x.jpg?v=1543432459",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbordeaux_small.jpg?v=1543432459"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeauxpois_560x.jpg?v=1543432459",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeauxpois_720x.jpg?v=1543432459",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeauxpois_966x.jpg?v=1543432459",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeauxpois_1200x.jpg?v=1543432459",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeauxpois_small.jpg?v=1543432459"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbourgognepois_560x.jpg?v=1543432459",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbourgognepois_720x.jpg?v=1543432459",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbourgognepois_966x.jpg?v=1543432459",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbourgognepois_1200x.jpg?v=1543432459",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbourgognepois_small.jpg?v=1543432459"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 71
        },

        {
          id: "1329786290218",
          description:
            "This burgundy tight is so easy to style! You can match it with almost everything  but just so you know, it looks particularly good with jeans fabric.",
          title: "Burgundy Tights",
          handle: "burgundy-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux-1_560x.jpg?v=1529681368",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux-1_720x.jpg?v=1529681368",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux-1_966x.jpg?v=1529681368",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux-1_1200x.jpg?v=1529681368",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux-1_small.jpg?v=1529681368"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bordeaux_560x.jpg?v=1529681368",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bordeaux_720x.jpg?v=1529681368",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bordeaux_966x.jpg?v=1529681368",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bordeaux_1200x.jpg?v=1529681368",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bordeaux_small.jpg?v=1529681368"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux2_560x.jpg?v=1529681368",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux2_720x.jpg?v=1529681368",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux2_966x.jpg?v=1529681368",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux2_1200x.jpg?v=1529681368",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux2_small.jpg?v=1529681368"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_560x.jpg?v=1529681368",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_720x.jpg?v=1529681368",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_966x.jpg?v=1529681368",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_1200x.jpg?v=1529681368",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeaux_small.jpg?v=1529681368"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 103
        },

        {
          id: "806479069226",
          description:
            "These tights are ultra versatile and can be dressed up or down, as you prefer!Made in ItalyStandard 100 certified by OEKO-TEXBoxer brief  No gussetAutomatic seams30 /60 Deniers Color : black",
          title: "Horizontal Knee Stripe Tights",
          handle: "horizontal-knee-stripe-tights",
          available: true,
          price: 1100,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bande_560x.jpg?v=1529681488",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bande_720x.jpg?v=1529681488",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bande_966x.jpg?v=1529681488",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bande_1200x.jpg?v=1529681488",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bande_small.jpg?v=1529681488"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_560x.jpg?v=1529681488",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_720x.jpg?v=1529681488",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_966x.jpg?v=1529681488",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_1200x.jpg?v=1529681488",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbandes_small.jpg?v=1529681488"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-bande_560x.jpg?v=1529681488",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-bande_720x.jpg?v=1529681488",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-bande_966x.jpg?v=1529681488",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-bande_1200x.jpg?v=1529681488",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-bande_small.jpg?v=1529681488"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bandes_560x.jpg?v=1529681488",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bandes_720x.jpg?v=1529681488",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bandes_966x.jpg?v=1529681488",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bandes_1200x.jpg?v=1529681488",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-2bandes_small.jpg?v=1529681488"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 132
        },

        {
          id: "1367560781866",
          description:
            "These cute, but simple tights are a wardrobe staple. Thanks to their fun print that isn't too over-the-top, you can dress them up or dress them down for just about any occasion. Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset30 DenierColor : black",
          title: "Triangle Print Tights",
          handle: "triangle-print-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-triangle2_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-triangle2_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-triangle2_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-triangle2_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-triangle2_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/triangle_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/triangle_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/triangle_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/triangle_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/triangle_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle1_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle1_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle1_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle1_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle1_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangles_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangles_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangles_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangles_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangles_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle2_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle2_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle2_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle2_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle2_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantriangle_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantriangle_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantriangle_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantriangle_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantriangle_small.jpg?v=1554395458"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle_560x.jpg?v=1554395458",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle_720x.jpg?v=1554395458",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle_966x.jpg?v=1554395458",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle_1200x.jpg?v=1554395458",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttriangle_small.jpg?v=1554395458"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 93
        },

        {
          id: "806530285610",
          description:
            "Our back heart seam tights are an ideal accessory to add a touch of femininity to all your looks. An absolute wardrobe must-have for parties and your every day style. Made in Italy Comfortable and resistantPanty Flat seam With cotton gusset30 DeniersColor : Black",
          title: "Back Heart Seam Tights",
          handle: "back-heart-seam-tights",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_cf70feb9-d840-42a2-a07c-b180b0caba46_560x.jpg?v=1529681273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_cf70feb9-d840-42a2-a07c-b180b0caba46_720x.jpg?v=1529681273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_cf70feb9-d840-42a2-a07c-b180b0caba46_966x.jpg?v=1529681273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_cf70feb9-d840-42a2-a07c-b180b0caba46_1200x.jpg?v=1529681273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_cf70feb9-d840-42a2-a07c-b180b0caba46_small.jpg?v=1529681273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_560x.jpg?v=1529681273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_720x.jpg?v=1529681273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_966x.jpg?v=1529681273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_1200x.jpg?v=1529681273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignecoeur_small.jpg?v=1529681273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur_560x.jpg?v=1529681273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur_720x.jpg?v=1529681273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur_966x.jpg?v=1529681273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur_1200x.jpg?v=1529681273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur_small.jpg?v=1529681273"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur3_560x.jpg?v=1529681273",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur3_720x.jpg?v=1529681273",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur3_966x.jpg?v=1529681273",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur3_1200x.jpg?v=1529681273",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-coeur3_small.jpg?v=1529681273"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 114
        },

        {
          id: "1367493509162",
          description:
            "These sheer tights have a heart print detailing located on the lower leg to add a subtle flirty touch to all your looks.  Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset30 DenierColor : Black",
          title: "Tights with Back Heart Print",
          handle: "tights-with-back-heart-print",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurmollet2_560x.jpg?v=1552596352",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurmollet2_720x.jpg?v=1552596352",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurmollet2_966x.jpg?v=1552596352",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurmollet2_1200x.jpg?v=1552596352",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeurmollet2_small.jpg?v=1552596352"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur3_560x.jpg?v=1552596352",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur3_720x.jpg?v=1552596352",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur3_966x.jpg?v=1552596352",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur3_1200x.jpg?v=1552596352",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur3_small.jpg?v=1552596352"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_560x.jpg?v=1552596352",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_720x.jpg?v=1552596352",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_966x.jpg?v=1552596352",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_1200x.jpg?v=1552596352",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeur_small.jpg?v=1552596352"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 25
        },

        {
          id: "1343768330282",
          description:
            "Our best-selling over-the-knee tights have received a modern update with polka dot detailing. Long story short, you need them in your wardrobe!Now available in extended sizes, these over-the-knee tights are designed with a special waistband that won’t pinch your waist and feature a control panty that will give you a chic fitted look.Made in ItalyBoxer brief  No gussetAutomatic seams30 /60 Deniers Color : black",
          title: "OTK Tights with Polka Dots (Plus Size)",
          handle: "over-the-knee-tights-with-polka-dot-detailing-plus-size",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes2_560x.jpg?v=1529682064",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes2_720x.jpg?v=1529682064",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes2_966x.jpg?v=1529682064",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes2_1200x.jpg?v=1529682064",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes2_small.jpg?v=1529682064"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCuissardePois_b075393a-664c-421a-b201-f28dbf19c3a2_560x.jpg?v=1529682064",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCuissardePois_b075393a-664c-421a-b201-f28dbf19c3a2_720x.jpg?v=1529682064",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCuissardePois_b075393a-664c-421a-b201-f28dbf19c3a2_966x.jpg?v=1529682064",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCuissardePois_b075393a-664c-421a-b201-f28dbf19c3a2_1200x.jpg?v=1529682064",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCuissardePois_b075393a-664c-421a-b201-f28dbf19c3a2_small.jpg?v=1529682064"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_560x.jpg?v=1529682064",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_720x.jpg?v=1529682064",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_966x.jpg?v=1529682064",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_1200x.jpg?v=1529682064",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collanttailleplus_small.jpg?v=1529682064"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes_560x.jpg?v=1529682064",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes_720x.jpg?v=1529682064",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes_966x.jpg?v=1529682064",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes_1200x.jpg?v=1529682064",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/taillepluscuissardes_small.jpg?v=1529682064"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantailleplus_560x.jpg?v=1529682064",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantailleplus_720x.jpg?v=1529682064",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantailleplus_966x.jpg?v=1529682064",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantailleplus_1200x.jpg?v=1529682064",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantailleplus_small.jpg?v=1529682064"
            }
          ],
          type: "Plus Size Tights",
          average_rating: 5.0,
          reviews_count: 36
        },

        {
          id: "1909913387050",
          description:
            "With their all-over chevron print, these tights are as versatile as they are fun. Add them to your wardrobe for a unique look. Made in Italy Comfortable and resistant90 DeniersColour: Mustard Yellow",
          title: "Textured Mustard Yellow Chevron Tights",
          handle: "textured-mustard-yellow-chevron-tights",
          available: false,
          price: 1800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantjaune_560x.jpg?v=1552429349",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantjaune_720x.jpg?v=1552429349",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantjaune_966x.jpg?v=1552429349",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantjaune_1200x.jpg?v=1552429349",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantjaune_small.jpg?v=1552429349"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmoutarde_560x.jpg?v=1552429349",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmoutarde_720x.jpg?v=1552429349",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmoutarde_966x.jpg?v=1552429349",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmoutarde_1200x.jpg?v=1552429349",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantmoutarde_small.jpg?v=1552429349"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/yellowtights_560x.jpg?v=1552429349",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/yellowtights_720x.jpg?v=1552429349",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/yellowtights_966x.jpg?v=1552429349",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/yellowtights_1200x.jpg?v=1552429349",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/yellowtights_small.jpg?v=1552429349"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 11
        },

        {
          id: "1350807584810",
          description:
            "Mustard is a colour that has definitely gained more popularity in recent years. These tights will be sure to brighten up any outfit.",
          title: "Mustard Yellow Tights",
          handle: "mustard-yellow-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde3.2_560x.jpg?v=1543269492",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde3.2_720x.jpg?v=1543269492",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde3.2_966x.jpg?v=1543269492",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde3.2_1200x.jpg?v=1543269492",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde3.2_small.jpg?v=1543269492"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde2.2_560x.jpg?v=1543269492",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde2.2_720x.jpg?v=1543269492",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde2.2_966x.jpg?v=1543269492",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde2.2_1200x.jpg?v=1543269492",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde2.2_small.jpg?v=1543269492"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde1_560x.jpg?v=1543269492",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde1_720x.jpg?v=1543269492",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde1_966x.jpg?v=1543269492",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde1_1200x.jpg?v=1543269492",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Moutarde1_small.jpg?v=1543269492"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 87
        },

        {
          id: "1591931568170",
          description:
            "Back seam tights get a modern and flirty update with back bow detailing with this original tights design. Made in Italy Comfortable and resistantMicromeshPanty With cotton gusset Flat seam30 DeniersColor : Black",
          title: "Bow Back Seam Tights",
          handle: "bow-and-back-seam",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_5d403870-1c0c-4221-9977-1ceb25278bf1_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_5d403870-1c0c-4221-9977-1ceb25278bf1_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_5d403870-1c0c-4221-9977-1ceb25278bf1_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_5d403870-1c0c-4221-9977-1ceb25278bf1_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_5d403870-1c0c-4221-9977-1ceb25278bf1_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights3_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights3_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights3_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights3_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights3_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_81730cb5-4f02-4e8f-bf36-b94ba38423f3_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_81730cb5-4f02-4e8f-bf36-b94ba38423f3_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_81730cb5-4f02-4e8f-bf36-b94ba38423f3_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_81730cb5-4f02-4e8f-bf36-b94ba38423f3_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_81730cb5-4f02-4e8f-bf36-b94ba38423f3_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights2_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights2_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights2_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights2_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bowtights2_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne2_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne2_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne2_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne2_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne2_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeud_small.jpg?v=1548693470"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeuds3_560x.jpg?v=1548693470",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeuds3_720x.jpg?v=1548693470",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeuds3_966x.jpg?v=1548693470",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeuds3_1200x.jpg?v=1548693470",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoeuds3_small.jpg?v=1548693470"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 57
        },

        {
          id: "806646317098",
          description:
            "These tights are the cat's meow!Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset40 DeniersColor : black",
          title: "Cat Tights",
          handle: "cat-tights",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-chat_560x.jpg?v=1529681401",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-chat_720x.jpg?v=1529681401",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-chat_966x.jpg?v=1529681401",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-chat_1200x.jpg?v=1529681401",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant-chat_small.jpg?v=1529681401"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchat_560x.jpg?v=1529681401",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchat_720x.jpg?v=1529681401",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchat_966x.jpg?v=1529681401",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchat_1200x.jpg?v=1529681401",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchat_small.jpg?v=1529681401"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 78
        },

        {
          id: "1347058991146",
          description:
            "Leg warmers are just what you need to survive the colder seasons in style! Made in Italy, these knee-length leg warmers are made of high quality fabrics that are guaranteed to keep you warm. Style meets function in this little accessory and it has never looked so good!  Note: fits maximum thigh size 20 inches; calf size 18 inches; height 28 inches",
          title: "Black Leg Warmers",
          handle: "black-leg-warmers",
          available: false,
          price: 1600,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambierenoir_560x.jpg?v=1548099449",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambierenoir_720x.jpg?v=1548099449",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambierenoir_966x.jpg?v=1548099449",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambierenoir_1200x.jpg?v=1548099449",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambierenoir_small.jpg?v=1548099449"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambieres_noires_560x.jpg?v=1548099449",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambieres_noires_720x.jpg?v=1548099449",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambieres_noires_966x.jpg?v=1548099449",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambieres_noires_1200x.jpg?v=1548099449",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambieres_noires_small.jpg?v=1548099449"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 39
        },

        {
          id: "1363753697322",
          description:
            "Navy blue is reminiscent of black, but the subtle difference can easily brighten of your wardrobe. ",
          title: "Dark Blue Tights",
          handle: "dark-blue-tights",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/navytights_560x.jpg?v=1571927848",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/navytights_720x.jpg?v=1571927848",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/navytights_966x.jpg?v=1571927848",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/navytights_1200x.jpg?v=1571927848",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/navytights_small.jpg?v=1571927848"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_bleu_560x.jpg?v=1571927848",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_bleu_720x.jpg?v=1571927848",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_bleu_966x.jpg?v=1571927848",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_bleu_1200x.jpg?v=1571927848",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Collant_bleu_small.jpg?v=1571927848"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_04a90ace-7757-4b79-a4b1-2bd556da744e_560x.jpg?v=1571927848",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_04a90ace-7757-4b79-a4b1-2bd556da744e_720x.jpg?v=1571927848",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_04a90ace-7757-4b79-a4b1-2bd556da744e_966x.jpg?v=1571927848",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_04a90ace-7757-4b79-a4b1-2bd556da744e_1200x.jpg?v=1571927848",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_04a90ace-7757-4b79-a4b1-2bd556da744e_small.jpg?v=1571927848"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bleu_560x.jpg?v=1571927848",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bleu_720x.jpg?v=1571927848",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bleu_966x.jpg?v=1571927848",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bleu_1200x.jpg?v=1571927848",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/bleu_small.jpg?v=1571927848"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbleu_560x.jpg?v=1571927817",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbleu_720x.jpg?v=1571927817",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbleu_966x.jpg?v=1571927817",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbleu_1200x.jpg?v=1571927817",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbleu_small.jpg?v=1571927817"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 21
        },

        {
          id: "1836120145962",
          description:
            "Super soft, these tights are ideal for winter and promise to keep you warm and comfortable.Made in Italy Comfortable and resistant270 DeniersColour: Black",
          title: "Super Opaque Black Tights",
          handle: "super-opaque-black-tights",
          available: false,
          price: 1600,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1_560x.jpg?v=1545885545",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1_720x.jpg?v=1545885545",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1_966x.jpg?v=1545885545",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1_1200x.jpg?v=1545885545",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1_small.jpg?v=1545885545"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1_560x.jpg?v=1545885554",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1_720x.jpg?v=1545885554",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1_966x.jpg?v=1545885554",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1_1200x.jpg?v=1545885554",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1_small.jpg?v=1545885554"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_560x.jpg?v=1545885563",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_720x.jpg?v=1545885563",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_966x.jpg?v=1545885563",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1200x.jpg?v=1545885563",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_small.jpg?v=1545885563"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_560x.jpg?v=1545885572",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_720x.jpg?v=1545885572",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_966x.jpg?v=1545885572",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_1200x.jpg?v=1545885572",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_small.jpg?v=1545885572"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_560x.jpg?v=1545885578",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_720x.jpg?v=1545885578",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_966x.jpg?v=1545885578",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_1200x.jpg?v=1545885578",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_small.jpg?v=1545885578"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 12
        },

        {
          id: "1763369648170",
          description:
            "These warm tights are a must to get you through the colder months! Super soft and extremely comfortable, you’ll want to wear them all winter long!Made in Italy Comfortable and resistantGussetFlat Seam 250 Denier",
          title: "Warm Black Floral Tights",
          handle: "black-warm-tights-flower",
          available: true,
          price: 2000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_de8263e2-cf54-4d06-8b05-91c9d303dbbf_560x.jpg?v=1545410198",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_de8263e2-cf54-4d06-8b05-91c9d303dbbf_720x.jpg?v=1545410198",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_de8263e2-cf54-4d06-8b05-91c9d303dbbf_966x.jpg?v=1545410198",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_de8263e2-cf54-4d06-8b05-91c9d303dbbf_1200x.jpg?v=1545410198",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_de8263e2-cf54-4d06-8b05-91c9d303dbbf_small.jpg?v=1545410198"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmblacktights_560x.jpg?v=1545410198",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmblacktights_720x.jpg?v=1545410198",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmblacktights_966x.jpg?v=1545410198",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmblacktights_1200x.jpg?v=1545410198",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmblacktights_small.jpg?v=1545410198"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackwarmtights_560x.jpg?v=1545410198",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackwarmtights_720x.jpg?v=1545410198",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackwarmtights_966x.jpg?v=1545410198",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackwarmtights_1200x.jpg?v=1545410198",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackwarmtights_small.jpg?v=1545410198"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_560x.jpg?v=1545410198",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_720x.jpg?v=1545410198",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_966x.jpg?v=1545410198",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_1200x.jpg?v=1545410198",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_small.jpg?v=1545410198"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 38
        },

        {
          id: "1836108546090",
          description:
            "Stay warm and stylish this winter in these warm tights featuring an all-over chevron print. They’re the perfect option to allow you to keep wearing skirts and dresses all season long. Made in Italy Comfortable and resistant",
          title: "Warm Black Chevron Tights",
          handle: "warm-black-chevron-tights",
          available: false,
          price: 2400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_1_560x.jpg?v=1545884799",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_1_720x.jpg?v=1545884799",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_1_966x.jpg?v=1545884799",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_1_1200x.jpg?v=1545884799",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/6_1_small.jpg?v=1545884799"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_1_560x.jpg?v=1545884808",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_1_720x.jpg?v=1545884808",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_1_966x.jpg?v=1545884808",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_1_1200x.jpg?v=1545884808",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_1_small.jpg?v=1545884808"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20-2_560x.jpg?v=1545884819",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20-2_720x.jpg?v=1545884819",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20-2_966x.jpg?v=1545884819",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20-2_1200x.jpg?v=1545884819",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20-2_small.jpg?v=1545884819"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21-3_560x.jpg?v=1545884827",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21-3_720x.jpg?v=1545884827",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21-3_966x.jpg?v=1545884827",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21-3_1200x.jpg?v=1545884827",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21-3_small.jpg?v=1545884827"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22-2_560x.jpg?v=1545884834",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22-2_720x.jpg?v=1545884834",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22-2_966x.jpg?v=1545884834",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22-2_1200x.jpg?v=1545884834",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22-2_small.jpg?v=1545884834"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 21
        },

        {
          id: "1836101861418",
          description:
            "Stay warm and stylish this winter in these warm tights featuring an all-over chevron print. They’re the perfect option to allow you to keep wearing skirts and dresses all season long. Made in Italy Comfortable and resistant",
          title: "Warm Grey Chevron Tights",
          handle: "warm-grey-chevron-tights",
          available: false,
          price: 2400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1_560x.jpg?v=1550163775",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1_720x.jpg?v=1550163775",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1_966x.jpg?v=1550163775",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1_1200x.jpg?v=1550163775",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1_small.jpg?v=1550163775"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1d1dc495-e0b6-4e75-be97-0525d5b10317_560x.jpg?v=1550163775",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1d1dc495-e0b6-4e75-be97-0525d5b10317_720x.jpg?v=1550163775",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1d1dc495-e0b6-4e75-be97-0525d5b10317_966x.jpg?v=1550163775",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1d1dc495-e0b6-4e75-be97-0525d5b10317_1200x.jpg?v=1550163775",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1d1dc495-e0b6-4e75-be97-0525d5b10317_small.jpg?v=1550163775"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_bfa99742-e868-46ea-b7d6-f2f81108101a_560x.jpg?v=1550163775",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_bfa99742-e868-46ea-b7d6-f2f81108101a_720x.jpg?v=1550163775",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_bfa99742-e868-46ea-b7d6-f2f81108101a_966x.jpg?v=1550163775",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_bfa99742-e868-46ea-b7d6-f2f81108101a_1200x.jpg?v=1550163775",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_bfa99742-e868-46ea-b7d6-f2f81108101a_small.jpg?v=1550163775"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_1_560x.jpg?v=1550163775",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_1_720x.jpg?v=1550163775",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_1_966x.jpg?v=1550163775",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_1_1200x.jpg?v=1550163775",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_1_small.jpg?v=1550163775"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 14
        },

        {
          id: "1347014721578",
          description:
            "Leg warmers are just what you need to survive the colder seasons in style! Made in Italy, these knee-length leg warmers are made of high quality fabrics that are guaranteed to keep you warm. Style meets function in this little accessory and it has never looked so good!  Note: fits maximum thigh size 20 inches; calf size 18 inches; height 28 inches",
          title: "Grey Leg Warmers",
          handle: "grey-leg-warmers",
          available: false,
          price: 2400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise2_560x.jpg?v=1548099481",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise2_720x.jpg?v=1548099481",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise2_966x.jpg?v=1548099481",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise2_1200x.jpg?v=1548099481",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise2_small.jpg?v=1548099481"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambiere_grise_560x.jpg?v=1548099481",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambiere_grise_720x.jpg?v=1548099481",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambiere_grise_966x.jpg?v=1548099481",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambiere_grise_1200x.jpg?v=1548099481",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Jambiere_grise_small.jpg?v=1548099481"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise_1_560x.jpg?v=1548099481",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise_1_720x.jpg?v=1548099481",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise_1_966x.jpg?v=1548099481",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise_1_1200x.jpg?v=1548099481",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/jambieregrise_1_small.jpg?v=1548099481"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 41
        },

        {
          id: "1725930274858",
          description:
            "These over-the-knee tights with grey detailing are a closet essential for the colder months. A winter version of our best-selling over-the-knee tights, this accessory will keep you warm and stylish all season long.Made in Italy Comfortable and resistant",
          title: "Warm Grey Over-the-Knee Tights",
          handle: "warm-otk-grey",
          available: true,
          price: 1600,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesgris_560x.jpg?v=1539277077",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesgris_720x.jpg?v=1539277077",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesgris_966x.jpg?v=1539277077",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesgris_1200x.jpg?v=1539277077",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardesgris_small.jpg?v=1539277077"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkgrischaud_560x.jpg?v=1539277077",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkgrischaud_720x.jpg?v=1539277077",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkgrischaud_966x.jpg?v=1539277077",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkgrischaud_1200x.jpg?v=1539277077",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkgrischaud_small.jpg?v=1539277077"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkhot_560x.jpg?v=1539277077",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkhot_720x.jpg?v=1539277077",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkhot_966x.jpg?v=1539277077",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkhot_1200x.jpg?v=1539277077",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkhot_small.jpg?v=1539277077"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 100
        },

        {
          id: "1761221312554",
          description:
            "Whether you’re a fan of the white polka dots or the over-the-knee tights style, you’re sure to fall hard for this hybrid! These OTK tights are are our most daring version yet.  Made in ItalyComfortable and resistantPantyFlat seamWith cotton gusset30 &amp; 60 DenierColor : black",
          title: "White Polka Dot Over-the-Knee Tights",
          handle: "otkkiko",
          available: true,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_6da50597-63ca-421e-a10a-bec2fcb15cb0_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_6da50597-63ca-421e-a10a-bec2fcb15cb0_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_6da50597-63ca-421e-a10a-bec2fcb15cb0_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_6da50597-63ca-421e-a10a-bec2fcb15cb0_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_6da50597-63ca-421e-a10a-bec2fcb15cb0_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_d7f98eb6-1f7e-4dcb-8514-abf732dc24e7_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_d7f98eb6-1f7e-4dcb-8514-abf732dc24e7_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_d7f98eb6-1f7e-4dcb-8514-abf732dc24e7_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_d7f98eb6-1f7e-4dcb-8514-abf732dc24e7_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_d7f98eb6-1f7e-4dcb-8514-abf732dc24e7_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_34bd6974-ebcd-47ba-abef-239f32b06a93_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_34bd6974-ebcd-47ba-abef-239f32b06a93_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_34bd6974-ebcd-47ba-abef-239f32b06a93_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_34bd6974-ebcd-47ba-abef-239f32b06a93_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcuissardes_34bd6974-ebcd-47ba-abef-239f32b06a93_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_27400279-803c-425f-a6bc-6bd964306edf_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_27400279-803c-425f-a6bc-6bd964306edf_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_27400279-803c-425f-a6bc-6bd964306edf_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_27400279-803c-425f-a6bc-6bd964306edf_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantotk_27400279-803c-425f-a6bc-6bd964306edf_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_342a3a8b-20cb-4d86-9965-fb1987f2d77e_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_342a3a8b-20cb-4d86-9965-fb1987f2d77e_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_342a3a8b-20cb-4d86-9965-fb1987f2d77e_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_342a3a8b-20cb-4d86-9965-fb1987f2d77e_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktights_342a3a8b-20cb-4d86-9965-fb1987f2d77e_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktightsdot_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktightsdot_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktightsdot_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktightsdot_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otktightsdot_small.jpg?v=1543585437"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwhitedot_560x.jpg?v=1543585437",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwhitedot_720x.jpg?v=1543585437",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwhitedot_966x.jpg?v=1543585437",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwhitedot_1200x.jpg?v=1543585437",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/otkwhitedot_small.jpg?v=1543585437"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 27
        },

        {
          id: "1347632562218",
          description:
            "This black opaque tights 50D will be your best ally of the season. Besides being extremely comfortable, these black tights are classy, elegant and a fashion must-have.",
          title: "Black Opaque Tights 50D",
          handle: "black-opaque-tights-50d",
          available: true,
          price: 1300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_31191f32-22c8-4670-a6a1-9d751e38f7cc_560x.jpg?v=1570221933",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_31191f32-22c8-4670-a6a1-9d751e38f7cc_720x.jpg?v=1570221933",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_31191f32-22c8-4670-a6a1-9d751e38f7cc_966x.jpg?v=1570221933",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_31191f32-22c8-4670-a6a1-9d751e38f7cc_1200x.jpg?v=1570221933",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_31191f32-22c8-4670-a6a1-9d751e38f7cc_small.jpg?v=1570221933"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_560x.jpg?v=1570221933",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_720x.jpg?v=1570221933",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_966x.jpg?v=1570221933",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_1200x.jpg?v=1570221933",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_small.jpg?v=1570221933"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_560x.jpg?v=1570221933",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_720x.jpg?v=1570221933",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_966x.jpg?v=1570221933",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_1200x.jpg?v=1570221933",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_small.jpg?v=1570221933"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_560x.jpg?v=1570221933",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_720x.jpg?v=1570221933",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_966x.jpg?v=1570221933",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_1200x.jpg?v=1570221933",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_small.jpg?v=1570221933"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1231020e-8839-410b-859d-29eea540926f_560x.jpg?v=1570221933",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1231020e-8839-410b-859d-29eea540926f_720x.jpg?v=1570221933",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1231020e-8839-410b-859d-29eea540926f_966x.jpg?v=1570221933",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1231020e-8839-410b-859d-29eea540926f_1200x.jpg?v=1570221933",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1231020e-8839-410b-859d-29eea540926f_small.jpg?v=1570221933"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 23
        },

        {
          id: "1763369812010",
          description:
            "These warm tights are a must to get you through the colder months! Super soft and extremely comfortable, you’ll want to wear them all winter long! Made in Italy Comfortable and resistantGussetFlat Seam 250 DEN",
          title: "Warm Burgundy Floral Tights",
          handle: "burgundy-warm-tights-flower",
          available: true,
          price: 2000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_4cb8b200-cd40-4fe4-abd6-d0b45220035f_560x.jpg?v=1545410294",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_4cb8b200-cd40-4fe4-abd6-d0b45220035f_720x.jpg?v=1545410294",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_4cb8b200-cd40-4fe4-abd6-d0b45220035f_966x.jpg?v=1545410294",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_4cb8b200-cd40-4fe4-abd6-d0b45220035f_1200x.jpg?v=1545410294",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtights_4cb8b200-cd40-4fe4-abd6-d0b45220035f_small.jpg?v=1545410294"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtighrs_560x.jpg?v=1545410294",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtighrs_720x.jpg?v=1545410294",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtighrs_966x.jpg?v=1545410294",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtighrs_1200x.jpg?v=1545410294",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmtighrs_small.jpg?v=1545410294"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaud_560x.jpg?v=1545410294",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaud_720x.jpg?v=1545410294",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaud_966x.jpg?v=1545410294",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaud_1200x.jpg?v=1545410294",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaud_small.jpg?v=1545410294"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmburtights_560x.jpg?v=1545410294",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmburtights_720x.jpg?v=1545410294",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmburtights_966x.jpg?v=1545410294",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmburtights_1200x.jpg?v=1545410294",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/warmburtights_small.jpg?v=1545410294"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaudhiver_560x.jpg?v=1545410294",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaudhiver_720x.jpg?v=1545410294",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaudhiver_966x.jpg?v=1545410294",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaudhiver_1200x.jpg?v=1545410294",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantchaudhiver_small.jpg?v=1545410294"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 18
        },

        {
          id: "1836069847082",
          description:
            "Stay warm and stylish this winter in these warm tights featuring an all-over chevron print. They’re the perfect option to allow you to keep wearing skirts and dresses all season long. Made in Italy Comfortable and resistant",
          title: "Warm Burgundy Chevron Tights",
          handle: "warm-burgundy-chevron-tights",
          available: true,
          price: 2400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux_1_560x.jpg?v=1548091734",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux_1_720x.jpg?v=1548091734",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux_1_966x.jpg?v=1548091734",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux_1_1200x.jpg?v=1548091734",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux_1_small.jpg?v=1548091734"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux2_15eb91e9-a535-41d1-b18d-b9b2354e9524_560x.jpg?v=1548091734",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux2_15eb91e9-a535-41d1-b18d-b9b2354e9524_720x.jpg?v=1548091734",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux2_15eb91e9-a535-41d1-b18d-b9b2354e9524_966x.jpg?v=1548091734",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux2_15eb91e9-a535-41d1-b18d-b9b2354e9524_1200x.jpg?v=1548091734",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux2_15eb91e9-a535-41d1-b18d-b9b2354e9524_small.jpg?v=1548091734"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux3_560x.jpg?v=1548091734",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux3_720x.jpg?v=1548091734",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux3_966x.jpg?v=1548091734",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux3_1200x.jpg?v=1548091734",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Bordeaux3_small.jpg?v=1548091734"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1_560x.jpg?v=1548091734",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1_720x.jpg?v=1548091734",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1_966x.jpg?v=1548091734",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1_1200x.jpg?v=1548091734",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1_small.jpg?v=1548091734"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1_560x.jpg?v=1548091734",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1_720x.jpg?v=1548091734",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1_966x.jpg?v=1548091734",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1_1200x.jpg?v=1548091734",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1_small.jpg?v=1548091734"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 24
        },

        {
          id: "1909917319210",
          description:
            "With their all-over chevron print, these tights are as versatile as they are fun. Add them to your wardrobe for a unique look. Made in Italy Comfortable and resistant90 DeniersColour: Plum",
          title: "Textured Plum Chevron Tights",
          handle: "textured-plum-chevron-tights",
          available: false,
          price: 1800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_560x.jpg?v=1552332643",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_720x.jpg?v=1552332643",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_966x.jpg?v=1552332643",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_1200x.jpg?v=1552332643",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_small.jpg?v=1552332643"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeau_560x.jpg?v=1552332645",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeau_720x.jpg?v=1552332645",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeau_966x.jpg?v=1552332645",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeau_1200x.jpg?v=1552332645",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantbordeau_small.jpg?v=1552332645"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 16
        },

        {
          id: "1909917974570",
          description:
            "With their all-over chevron print, these tights are as versatile as they are fun. Add them to your wardrobe for a unique look. Made in Italy Comfortable and resistant90 DeniersColour: Emerald ",
          title: "Textured Emerald Chevron Tights",
          handle: "textured-green-chevron-tights",
          available: false,
          price: 1800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvertforet_560x.jpg?v=1552429414",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvertforet_720x.jpg?v=1552429414",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvertforet_966x.jpg?v=1552429414",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvertforet_1200x.jpg?v=1552429414",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvertforet_small.jpg?v=1552429414"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_b17b0e32-6a52-4e55-a7f1-a17de831fb13_560x.jpg?v=1552429414",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_b17b0e32-6a52-4e55-a7f1-a17de831fb13_720x.jpg?v=1552429414",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_b17b0e32-6a52-4e55-a7f1-a17de831fb13_966x.jpg?v=1552429414",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_b17b0e32-6a52-4e55-a7f1-a17de831fb13_1200x.jpg?v=1552429414",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/colortights_b17b0e32-6a52-4e55-a7f1-a17de831fb13_small.jpg?v=1552429414"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvert_560x.jpg?v=1552429414",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvert_720x.jpg?v=1552429414",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvert_966x.jpg?v=1552429414",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvert_1200x.jpg?v=1552429414",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantvert_small.jpg?v=1552429414"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/greentights_560x.jpg?v=1552429414",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/greentights_720x.jpg?v=1552429414",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/greentights_966x.jpg?v=1552429414",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/greentights_1200x.jpg?v=1552429414",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/greentights_small.jpg?v=1552429414"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 14
        },

        {
          id: "1591932289066",
          description:
            "Navy tights are updated with white polka dots this season, offering a new opaque tights style, you’ll love to have in your wardrobe. Made in Italy Comfortable and resistantFlat seam With cotton gusset40 DeniersColor : navy blue",
          title: "Navy Polka Dot Tights",
          handle: "navy-tights-with-white-polka-dots",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbleu_560x.jpg?v=1533571587",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbleu_720x.jpg?v=1533571587",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbleu_966x.jpg?v=1533571587",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbleu_1200x.jpg?v=1533571587",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisbleu_small.jpg?v=1533571587"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisnavy_560x.jpg?v=1533571605",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisnavy_720x.jpg?v=1533571605",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisnavy_966x.jpg?v=1533571605",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisnavy_1200x.jpg?v=1533571605",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoisnavy_small.jpg?v=1533571605"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnavydot_560x.jpg?v=1533571619",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnavydot_720x.jpg?v=1533571619",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnavydot_966x.jpg?v=1533571619",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnavydot_1200x.jpg?v=1533571619",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnavydot_small.jpg?v=1533571619"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 42
        },

        {
          id: "1365695561770",
          description:
            "Sexy meets fun with these sheer tights featuring a zigzag back seam. These tights are all business in the front and all party in the back!Made in ItalyComfortable and resistantPanty30 DenierColour: Black",
          title: "Back Seam Zigzag Tights",
          handle: "back-seam-zigzag-tights",
          available: false,
          price: 1200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag4_560x.jpg?v=1529606744",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag4_720x.jpg?v=1529606744",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag4_966x.jpg?v=1529606744",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag4_1200x.jpg?v=1529606744",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag4_small.jpg?v=1529606744"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag2-1_560x.jpg?v=1529606744",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag2-1_720x.jpg?v=1529606744",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag2-1_966x.jpg?v=1529606744",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag2-1_1200x.jpg?v=1529606744",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantzigzag2-1_small.jpg?v=1529606744"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 26
        },

        {
          id: "1426344280106",
          description:
            "Polka dots are reinvented with these tights featuring a dotted pattern in shades of aqua blue. They are sure to provide you with a notice-me look!Made in ItalyColour : Black &amp; Colored Dots20 Deniers",
          title: "Tights with Aqua Dotted Pattern",
          handle: "tights-with-aqua-dotted-pattern",
          available: false,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a8a86c52-ece2-4f88-8e44-e428211fc7d8_560x.jpg?v=1532448263",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a8a86c52-ece2-4f88-8e44-e428211fc7d8_720x.jpg?v=1532448263",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a8a86c52-ece2-4f88-8e44-e428211fc7d8_966x.jpg?v=1532448263",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a8a86c52-ece2-4f88-8e44-e428211fc7d8_1200x.jpg?v=1532448263",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_a8a86c52-ece2-4f88-8e44-e428211fc7d8_small.jpg?v=1532448263"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dots_560x.jpg?v=1532448263",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dots_720x.jpg?v=1532448263",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dots_966x.jpg?v=1532448263",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dots_1200x.jpg?v=1532448263",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dots_small.jpg?v=1532448263"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_fd34a485-9136-41cd-bd47-2cee2b1db72b_560x.jpg?v=1532448263",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_fd34a485-9136-41cd-bd47-2cee2b1db72b_720x.jpg?v=1532448263",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_fd34a485-9136-41cd-bd47-2cee2b1db72b_966x.jpg?v=1532448263",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_fd34a485-9136-41cd-bd47-2cee2b1db72b_1200x.jpg?v=1532448263",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpois_fd34a485-9136-41cd-bd47-2cee2b1db72b_small.jpg?v=1532448263"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoiscolor_560x.jpg?v=1532448263",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoiscolor_720x.jpg?v=1532448263",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoiscolor_966x.jpg?v=1532448263",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoiscolor_1200x.jpg?v=1532448263",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantpoiscolor_small.jpg?v=1532448263"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 41
        },

        {
          id: "1763369877546",
          description:
            "Glitter Tights are the ultimate way to make a statement this Holiday season. Add them to all your outfits for a party ready look. Made in ItalyColor : BlackComfortable and resistantGusset &amp; PantyFlat seam Lurex 50 Denier",
          title: "Glitter Tights",
          handle: "lurex",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex_560x.jpg?v=1543267475",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex_720x.jpg?v=1543267475",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex_966x.jpg?v=1543267475",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex_1200x.jpg?v=1543267475",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex_small.jpg?v=1543267475"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex2_560x.jpg?v=1543267475",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex2_720x.jpg?v=1543267475",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex2_966x.jpg?v=1543267475",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex2_1200x.jpg?v=1543267475",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlurex2_small.jpg?v=1543267475"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/lurextights_560x.jpg?v=1543267475",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/lurextights_720x.jpg?v=1543267475",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/lurextights_966x.jpg?v=1543267475",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/lurextights_1200x.jpg?v=1543267475",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/lurextights_small.jpg?v=1543267475"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/christmastights_560x.jpg?v=1543267475",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/christmastights_720x.jpg?v=1543267475",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/christmastights_966x.jpg?v=1543267475",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/christmastights_1200x.jpg?v=1543267475",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/christmastights_small.jpg?v=1543267475"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shinytights_560x.jpg?v=1543267475",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shinytights_720x.jpg?v=1543267475",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shinytights_966x.jpg?v=1543267475",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shinytights_1200x.jpg?v=1543267475",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shinytights_small.jpg?v=1543267475"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 40
        },

        {
          id: "1347699048490",
          description: "These semi-sheer tights are a classic in any wardrobe.",
          title: "Semi-Sheer Tights 30D",
          handle: "semi-sheer-tights-30d",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_560x.jpg?v=1521140758",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_720x.jpg?v=1521140758",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_966x.jpg?v=1521140758",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_1200x.jpg?v=1521140758",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_small.jpg?v=1521140758"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 9
        },

        {
          id: "1369101959210",
          description:
            "Fishnet Tights are the perfect tights option to take you through the warmer season. A classic and versatile tights style, your skirts, cut-off shorts and ripped jeans will thank you for! ",
          title: "Fishnet Tights",
          handle: "fishnet-tights",
          available: false,
          price: 1100,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fischnettights_560x.jpg?v=1529683059",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fischnettights_720x.jpg?v=1529683059",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fischnettights_966x.jpg?v=1529683059",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fischnettights_1200x.jpg?v=1529683059",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fischnettights_small.jpg?v=1529683059"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/image-produit-2_560x.jpg?v=1529683059",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/image-produit-2_720x.jpg?v=1529683059",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/image-produit-2_966x.jpg?v=1529683059",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/image-produit-2_1200x.jpg?v=1529683059",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/image-produit-2_small.jpg?v=1529683059"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresilleMoyenne_560x.jpg?v=1529683059",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresilleMoyenne_720x.jpg?v=1529683059",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresilleMoyenne_966x.jpg?v=1529683059",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresilleMoyenne_1200x.jpg?v=1529683059",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresilleMoyenne_small.jpg?v=1529683059"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille2_560x.jpg?v=1534772679",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille2_720x.jpg?v=1534772679",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille2_966x.jpg?v=1534772679",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille2_1200x.jpg?v=1534772679",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille2_small.jpg?v=1534772679"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnettights2_560x.jpg?v=1529683059",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnettights2_720x.jpg?v=1529683059",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnettights2_966x.jpg?v=1529683059",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnettights2_1200x.jpg?v=1529683059",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnettights2_small.jpg?v=1529683059"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille_560x.jpg?v=1534772679",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille_720x.jpg?v=1534772679",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille_966x.jpg?v=1534772679",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille_1200x.jpg?v=1534772679",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantresille_small.jpg?v=1534772679"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 11
        },

        {
          id: "1702283444266",
          description:
            "These black opaque tights 50D will be your best ally this season. Besides being extremely comfortable, these black tights are classy, elegant and a fashion must-have. With this pack, you’ll get 3 pairs of tights for the price of 2!",
          title: "Black Opaque Tights 50D (3 Pack)",
          handle: "3-pack-black-opaque-tights-50d",
          available: true,
          price: 2500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_00fb456f-60da-45b4-9b7f-fdb3052546cd_560x.jpg?v=1570221796",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_00fb456f-60da-45b4-9b7f-fdb3052546cd_720x.jpg?v=1570221796",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_00fb456f-60da-45b4-9b7f-fdb3052546cd_966x.jpg?v=1570221796",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_00fb456f-60da-45b4-9b7f-fdb3052546cd_1200x.jpg?v=1570221796",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/opaquetights_00fb456f-60da-45b4-9b7f-fdb3052546cd_small.jpg?v=1570221796"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_16e483f3-2a30-4454-a426-6e5d8d2eeb32_560x.jpg?v=1570221796",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_16e483f3-2a30-4454-a426-6e5d8d2eeb32_720x.jpg?v=1570221796",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_16e483f3-2a30-4454-a426-6e5d8d2eeb32_966x.jpg?v=1570221796",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_16e483f3-2a30-4454-a426-6e5d8d2eeb32_1200x.jpg?v=1570221796",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant50d_16e483f3-2a30-4454-a426-6e5d8d2eeb32_small.jpg?v=1570221796"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_560x.jpg?v=1570221796",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_720x.jpg?v=1570221796",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_966x.jpg?v=1570221796",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_1200x.jpg?v=1570221796",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blacktights_small.jpg?v=1570221796"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_a2fbb813-c287-4618-bfc3-1daed439d782_560x.jpg?v=1570221796",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_a2fbb813-c287-4618-bfc3-1daed439d782_720x.jpg?v=1570221796",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_a2fbb813-c287-4618-bfc3-1daed439d782_966x.jpg?v=1570221796",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_a2fbb813-c287-4618-bfc3-1daed439d782_1200x.jpg?v=1570221796",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantnoir_a2fbb813-c287-4618-bfc3-1daed439d782_small.jpg?v=1570221796"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_e877d918-2342-4e1c-82ca-2e0569065184_560x.jpg?v=1570221796",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_e877d918-2342-4e1c-82ca-2e0569065184_720x.jpg?v=1570221796",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_e877d918-2342-4e1c-82ca-2e0569065184_966x.jpg?v=1570221796",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_e877d918-2342-4e1c-82ca-2e0569065184_1200x.jpg?v=1570221796",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collant_uni_e877d918-2342-4e1c-82ca-2e0569065184_small.jpg?v=1570221796"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 2
        },

        {
          id: "1763370008618",
          description:
            "Glitter Back Seam Tights offer a glamorous look that’s perfect to complete your Holiday outfits. Add a romantic and sexy touch to your looks with this simple accessory. Made in Italy Comfortable and resistantNo pantyNo gussetFlat seam Lurex back seam 40 Denier ",
          title: "Glitter Back Seam Tights",
          handle: "lurex-beckseam",
          available: true,
          price: 1500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_a89a71db-e916-473d-be43-9a3f182fa7f9_560x.jpg?v=1543267866",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_a89a71db-e916-473d-be43-9a3f182fa7f9_720x.jpg?v=1543267866",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_a89a71db-e916-473d-be43-9a3f182fa7f9_966x.jpg?v=1543267866",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_a89a71db-e916-473d-be43-9a3f182fa7f9_1200x.jpg?v=1543267866",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantligne_a89a71db-e916-473d-be43-9a3f182fa7f9_small.jpg?v=1543267866"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignearriere_560x.jpg?v=1543267869",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignearriere_720x.jpg?v=1543267869",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignearriere_966x.jpg?v=1543267869",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignearriere_1200x.jpg?v=1543267869",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantlignearriere_small.jpg?v=1543267869"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 33
        },

        {
          id: "806736199722",
          description:
            "These socks with glitter effect will seriously jazz up your outfits! Made to last with a blend of  high quality fabrics, they’re also as comfortable as they are chic.",
          title: "Burgundy Glitter Ankle Socks",
          handle: "burgundy-glitter-ankle-socks",
          available: true,
          price: 800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettepaillettebordeaux_560x.jpg?v=1529595944",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettepaillettebordeaux_720x.jpg?v=1529595944",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettepaillettebordeaux_966x.jpg?v=1529595944",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettepaillettebordeaux_1200x.jpg?v=1529595944",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettepaillettebordeaux_small.jpg?v=1529595944"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks2_d5b39d33-e425-45ce-a730-14ef8b3b6323_560x.jpg?v=1529595944",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks2_d5b39d33-e425-45ce-a730-14ef8b3b6323_720x.jpg?v=1529595944",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks2_d5b39d33-e425-45ce-a730-14ef8b3b6323_966x.jpg?v=1529595944",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks2_d5b39d33-e425-45ce-a730-14ef8b3b6323_1200x.jpg?v=1529595944",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks2_d5b39d33-e425-45ce-a730-14ef8b3b6323_small.jpg?v=1529595944"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks_44540bbe-d613-4c4d-9b07-dc8574802230_560x.jpg?v=1529595944",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks_44540bbe-d613-4c4d-9b07-dc8574802230_720x.jpg?v=1529595944",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks_44540bbe-d613-4c4d-9b07-dc8574802230_966x.jpg?v=1529595944",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks_44540bbe-d613-4c4d-9b07-dc8574802230_1200x.jpg?v=1529595944",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/glitter-socks_44540bbe-d613-4c4d-9b07-dc8574802230_small.jpg?v=1529595944"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 7
        },

        {
          id: "807121354794",
          description:
            "*High quality &amp; ultra comfort*These leggings are smart enough that you can easily wear them from a group study session to the bar for some drinks with friends.",
          title: "Faux Leather with Strips Leggings",
          handle: "faux-leather-with-strips",
          available: false,
          price: 5500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_560x.jpg?v=1529681759",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_720x.jpg?v=1529681759",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_966x.jpg?v=1529681759",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1200x.jpg?v=1529681759",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_small.jpg?v=1529681759"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingcuissarde_560x.jpg?v=1529681759",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingcuissarde_720x.jpg?v=1529681759",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingcuissarde_966x.jpg?v=1529681759",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingcuissarde_1200x.jpg?v=1529681759",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingcuissarde_small.jpg?v=1529681759"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_560x.jpg?v=1529681759",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_720x.jpg?v=1529681759",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_966x.jpg?v=1529681759",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1200x.jpg?v=1529681759",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_small.jpg?v=1529681759"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 6
        },

        {
          id: "1963680399402",
          description:
            "Sleep in comfort and style with these super soft pyjama shorts that fit like a second skin. Made with NILIT cooling yarn, it will leave you feeling refreshed and comfortable when the temperatures rise.Made in ItalySuper softMade with NILIT fibres proven to lower body temperature Colour: black",
          title: "Pyjama Shorts",
          handle: "short-de-nuit-noir",
          available: true,
          price: 2200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/short_de_nuit-2_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/short_de_nuit-2_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/short_de_nuit-2_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/short_de_nuit-2_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/short_de_nuit-2_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_de_nuit-1_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_de_nuit-1_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_de_nuit-1_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_de_nuit-1_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_de_nuit-1_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortnuit_096a30bf-7d3d-4c78-b92e-b46f634729ef_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortnuit_096a30bf-7d3d-4c78-b92e-b46f634729ef_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortnuit_096a30bf-7d3d-4c78-b92e-b46f634729ef_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortnuit_096a30bf-7d3d-4c78-b92e-b46f634729ef_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortnuit_096a30bf-7d3d-4c78-b92e-b46f634729ef_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort2_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort2_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort2_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort2_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort2_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pyjamashort_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_c7335fae-05ed-4494-a7c2-e6aefe51e5f9_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_c7335fae-05ed-4494-a7c2-e6aefe51e5f9_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_c7335fae-05ed-4494-a7c2-e6aefe51e5f9_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_c7335fae-05ed-4494-a7c2-e6aefe51e5f9_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_c7335fae-05ed-4494-a7c2-e6aefe51e5f9_small.jpg?v=1571927932"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_560x.jpg?v=1571927932",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_720x.jpg?v=1571927932",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_966x.jpg?v=1571927932",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_1200x.jpg?v=1571927932",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortpyjama_small.jpg?v=1571927932"
            }
          ],
          type: "Leggings",
          average_rating: 4.5,
          reviews_count: 9
        },

        {
          id: "1963683020842",
          description:
            "Up your athleisure game with these everyday leggings. Featuring a subtle, yet eye-catching floral print finish, these leggings will quickly become your new favourite wardrobe essential. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Black Floral Print Legging",
          handle: "legging-floral-noir",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackleggings_560x.jpg?v=1563198776",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackleggings_720x.jpg?v=1563198776",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackleggings_966x.jpg?v=1563198776",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackleggings_1200x.jpg?v=1563198776",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackleggings_small.jpg?v=1563198776"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1365752a-2aae-45ac-a8b0-fa05d969b047_560x.jpg?v=1563198776",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1365752a-2aae-45ac-a8b0-fa05d969b047_720x.jpg?v=1563198776",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1365752a-2aae-45ac-a8b0-fa05d969b047_966x.jpg?v=1563198776",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1365752a-2aae-45ac-a8b0-fa05d969b047_1200x.jpg?v=1563198776",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1365752a-2aae-45ac-a8b0-fa05d969b047_small.jpg?v=1563198776"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_029c6460-06f6-43e7-8faa-d3ee22ff1f35_560x.jpg?v=1563198776",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_029c6460-06f6-43e7-8faa-d3ee22ff1f35_720x.jpg?v=1563198776",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_029c6460-06f6-43e7-8faa-d3ee22ff1f35_966x.jpg?v=1563198776",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_029c6460-06f6-43e7-8faa-d3ee22ff1f35_1200x.jpg?v=1563198776",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/28_029c6460-06f6-43e7-8faa-d3ee22ff1f35_small.jpg?v=1563198776"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_0b508c75-8b6a-4b46-93c4-1822f9188af8_560x.jpg?v=1563198776",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_0b508c75-8b6a-4b46-93c4-1822f9188af8_720x.jpg?v=1563198776",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_0b508c75-8b6a-4b46-93c4-1822f9188af8_966x.jpg?v=1563198776",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_0b508c75-8b6a-4b46-93c4-1822f9188af8_1200x.jpg?v=1563198776",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_0b508c75-8b6a-4b46-93c4-1822f9188af8_small.jpg?v=1563198776"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingnoir1_560x.jpg?v=1563198776",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingnoir1_720x.jpg?v=1563198776",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingnoir1_966x.jpg?v=1563198776",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingnoir1_1200x.jpg?v=1563198776",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingnoir1_small.jpg?v=1563198776"
            }
          ],
          type: "Leggings",
          average_rating: 4.5,
          reviews_count: 3
        },

        {
          id: "1963683446826",
          description:
            "A comfortable athleisure essential, these versatile shorts are ideal for workouts or for everyday lounging. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Khaki Athletic Shorts",
          handle: "shorty-kaki",
          available: true,
          price: 1800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_560x.jpg?v=1563198332",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_720x.jpg?v=1563198332",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_966x.jpg?v=1563198332",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_1200x.jpg?v=1563198332",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_small.jpg?v=1563198332"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_652b1bc6-11c1-46c3-b160-42efba17c47b_560x.jpg?v=1563198332",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_652b1bc6-11c1-46c3-b160-42efba17c47b_720x.jpg?v=1563198332",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_652b1bc6-11c1-46c3-b160-42efba17c47b_966x.jpg?v=1563198332",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_652b1bc6-11c1-46c3-b160-42efba17c47b_1200x.jpg?v=1563198332",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki_652b1bc6-11c1-46c3-b160-42efba17c47b_small.jpg?v=1563198332"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki1_560x.jpg?v=1563198332",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki1_720x.jpg?v=1563198332",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki1_966x.jpg?v=1563198332",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki1_1200x.jpg?v=1563198332",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki1_small.jpg?v=1563198332"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki2_560x.jpg?v=1563198332",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki2_720x.jpg?v=1563198332",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki2_966x.jpg?v=1563198332",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki2_1200x.jpg?v=1563198332",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortkaki2_small.jpg?v=1563198332"
            }
          ],
          type: "Leggings",
          average_rating: 4.5,
          reviews_count: 8
        },

        {
          id: "1963683217450",
          description:
            "Up your athleisure game with these everyday leggings. Featuring a subtle, yet eye-catching floral print finish, these leggings will quickly become your new favourite wardrobe essential. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Khaki Floral Print Legging",
          handle: "legging-floral-kaki",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_560x.jpg?v=1563198094",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_720x.jpg?v=1563198094",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_966x.jpg?v=1563198094",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_1200x.jpg?v=1563198094",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_small.jpg?v=1563198094"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki2_560x.jpg?v=1563198094",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki2_720x.jpg?v=1563198094",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki2_966x.jpg?v=1563198094",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki2_1200x.jpg?v=1563198094",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki2_small.jpg?v=1563198094"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_9ce04c42-3067-4e4e-9478-f2d04f000ad9_560x.jpg?v=1563198094",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_9ce04c42-3067-4e4e-9478-f2d04f000ad9_720x.jpg?v=1563198094",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_9ce04c42-3067-4e4e-9478-f2d04f000ad9_966x.jpg?v=1563198094",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_9ce04c42-3067-4e4e-9478-f2d04f000ad9_1200x.jpg?v=1563198094",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingskaki_9ce04c42-3067-4e4e-9478-f2d04f000ad9_small.jpg?v=1563198094"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki1.2_560x.jpg?v=1563198094",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki1.2_720x.jpg?v=1563198094",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki1.2_966x.jpg?v=1563198094",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki1.2_1200x.jpg?v=1563198094",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki1.2_small.jpg?v=1563198094"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki2_560x.jpg?v=1563198094",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki2_720x.jpg?v=1563198094",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki2_966x.jpg?v=1563198094",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki2_1200x.jpg?v=1563198094",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingkaki2_small.jpg?v=1563198094"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 4
        },

        {
          id: "1963683348522",
          description:
            "A comfortable athleisure essential, these versatile shorts are ideal for workouts or for everyday lounging. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Black Athletic Shorts",
          handle: "shorty-noir",
          available: false,
          price: 1800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackshort_560x.jpg?v=1563197966",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackshort_720x.jpg?v=1563197966",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackshort_966x.jpg?v=1563197966",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackshort_1200x.jpg?v=1563197966",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/blackshort_small.jpg?v=1563197966"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_1.2_1_560x.jpg?v=1563197966",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_1.2_1_720x.jpg?v=1563197966",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_1.2_1_966x.jpg?v=1563197966",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_1.2_1_1200x.jpg?v=1563197966",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short_1.2_1_small.jpg?v=1563197966"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short2_560x.jpg?v=1563197966",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short2_720x.jpg?v=1563197966",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short2_966x.jpg?v=1563197966",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short2_1200x.jpg?v=1563197966",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Short2_small.jpg?v=1563197966"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 6
        },

        {
          id: "1963682955306",
          description:
            "Up your athleisure game with these everyday leggings. Featuring a subtle, yet eye-catching floral print finish, these leggings will quickly become your new favourite wardrobe essential. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Rose Floral Print Legging",
          handle: "legging-floral-rose",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_560x.jpg?v=1563198715",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_720x.jpg?v=1563198715",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_966x.jpg?v=1563198715",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_1200x.jpg?v=1563198715",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_small.jpg?v=1563198715"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrose_560x.jpg?v=1563198715",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrose_720x.jpg?v=1563198715",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrose_966x.jpg?v=1563198715",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrose_1200x.jpg?v=1563198715",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrose_small.jpg?v=1563198715"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pinkleggings_560x.jpg?v=1563198715",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pinkleggings_720x.jpg?v=1563198715",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pinkleggings_966x.jpg?v=1563198715",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pinkleggings_1200x.jpg?v=1563198715",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/pinkleggings_small.jpg?v=1563198715"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_6ba3c588-6e7d-4bab-b725-cedbf6308284_560x.jpg?v=1563198715",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_6ba3c588-6e7d-4bab-b725-cedbf6308284_720x.jpg?v=1563198715",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_6ba3c588-6e7d-4bab-b725-cedbf6308284_966x.jpg?v=1563198715",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_6ba3c588-6e7d-4bab-b725-cedbf6308284_1200x.jpg?v=1563198715",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingrose_6ba3c588-6e7d-4bab-b725-cedbf6308284_small.jpg?v=1563198715"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-rose-3_560x.jpg?v=1563198715",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-rose-3_720x.jpg?v=1563198715",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-rose-3_966x.jpg?v=1563198715",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-rose-3_1200x.jpg?v=1563198715",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-rose-3_small.jpg?v=1563198715"
            }
          ],
          type: "Leggings",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1963683250218",
          description:
            "Up your athleisure game with these everyday leggings. Featuring a subtle, yet eye-catching floral print finish, these leggings will quickly become your new favourite wardrobe essential. Made in ItalyMade with high technology yarnsSeamless lightweight fabricBreathableStandard 100 certified by OEKO-TEXCertified free of harmful chemicals",
          title: "Raspberry Floral Print Legging",
          handle: "legging-floral-fushia",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redleggings_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redleggings_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redleggings_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redleggings_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redleggings_small.jpg?v=1563198527"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrouge_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrouge_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrouge_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrouge_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrouge_small.jpg?v=1563198527"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrasbery_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrasbery_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrasbery_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrasbery_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsrasbery_small.jpg?v=1563198527"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-1_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-1_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-1_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-1_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-1_small.jpg?v=1563198527"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-2.2_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-2.2_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-2.2_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-2.2_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-2.2_small.jpg?v=1563198527"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-3_560x.jpg?v=1563198527",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-3_720x.jpg?v=1563198527",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-3_966x.jpg?v=1563198527",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-3_1200x.jpg?v=1563198527",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/Leg-red-3_small.jpg?v=1563198527"
            }
          ],
          type: "Leggings",
          average_rating: 4.0,
          reviews_count: 5
        },

        {
          id: "1577134293034",
          description:
            "These no-show, low-cut socks are as cute as they are comfortable! Designed to stay hidden, they have an interior grip and an exclusive and fun design. They’re a basic that’s well… anything, but basic! Pack of 3 One sizeFeatures interior grip to prevent them from slipping down",
          title: "Graphic No-Show Socks (3 pack)",
          handle: "3-short-socks-pack",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks_de3863e2-0252-4df7-b1ed-ef8a74390da6_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks_de3863e2-0252-4df7-b1ed-ef8a74390da6_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks_de3863e2-0252-4df7-b1ed-ef8a74390da6_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks_de3863e2-0252-4df7-b1ed-ef8a74390da6_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks_de3863e2-0252-4df7-b1ed-ef8a74390da6_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksback_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksback_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksback_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksback_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksback_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_fc477db6-cb83-4ef7-a199-06160cf52f8e_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_fc477db6-cb83-4ef7-a199-06160cf52f8e_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_fc477db6-cb83-4ef7-a199-06160cf52f8e_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_fc477db6-cb83-4ef7-a199-06160cf52f8e_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_fc477db6-cb83-4ef7-a199-06160cf52f8e_small.jpg?v=1532523648"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_a7d15376-c073-4a4d-9bc8-6ff6c790b1a2_560x.jpg?v=1532523648",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_a7d15376-c073-4a4d-9bc8-6ff6c790b1a2_720x.jpg?v=1532523648",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_a7d15376-c073-4a4d-9bc8-6ff6c790b1a2_966x.jpg?v=1532523648",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_a7d15376-c073-4a4d-9bc8-6ff6c790b1a2_1200x.jpg?v=1532523648",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_a7d15376-c073-4a4d-9bc8-6ff6c790b1a2_small.jpg?v=1532523648"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 11
        },

        {
          id: "1578006937642",
          description:
            "These no-show and ankle socks are as cute as they are comfortable! They’re a basic that’s well… anything, but basic!Pack of 53 No-Show Socks2 Ankle SocksOne sizeFeatures interior grip to prevent them from slipping down",
          title: "Graphic Ankle & No-Show Socks (5 Pack)",
          handle: "5-socks-pack",
          available: true,
          price: 2200,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_7da7c8a3-46a8-429e-b017-493e5de89fcb_560x.jpg?v=1532544335",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_7da7c8a3-46a8-429e-b017-493e5de89fcb_720x.jpg?v=1532544335",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_7da7c8a3-46a8-429e-b017-493e5de89fcb_966x.jpg?v=1532544335",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_7da7c8a3-46a8-429e-b017-493e5de89fcb_1200x.jpg?v=1532544335",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/dotsocks_7da7c8a3-46a8-429e-b017-493e5de89fcb_small.jpg?v=1532544335"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_f107e2f2-d342-4f4e-b428-3d0e9102da9f_560x.jpg?v=1532544335",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_f107e2f2-d342-4f4e-b428-3d0e9102da9f_720x.jpg?v=1532544335",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_f107e2f2-d342-4f4e-b428-3d0e9102da9f_966x.jpg?v=1532544335",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_f107e2f2-d342-4f4e-b428-3d0e9102da9f_1200x.jpg?v=1532544335",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/cutesocks_f107e2f2-d342-4f4e-b428-3d0e9102da9f_small.jpg?v=1532544335"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_c42e94eb-f678-40c9-b3bb-9894f4a8d13d_560x.jpg?v=1532544335",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_c42e94eb-f678-40c9-b3bb-9894f4a8d13d_720x.jpg?v=1532544335",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_c42e94eb-f678-40c9-b3bb-9894f4a8d13d_966x.jpg?v=1532544335",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_c42e94eb-f678-40c9-b3bb-9894f4a8d13d_1200x.jpg?v=1532544335",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/redsocks_c42e94eb-f678-40c9-b3bb-9894f4a8d13d_small.jpg?v=1532544335"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes_7fd29399-3af6-4d51-a350-5f5d1f40d136_560x.jpg?v=1532544335",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes_7fd29399-3af6-4d51-a350-5f5d1f40d136_720x.jpg?v=1532544335",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes_7fd29399-3af6-4d51-a350-5f5d1f40d136_966x.jpg?v=1532544335",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes_7fd29399-3af6-4d51-a350-5f5d1f40d136_1200x.jpg?v=1532544335",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes_7fd29399-3af6-4d51-a350-5f5d1f40d136_small.jpg?v=1532544335"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes2_560x.jpg?v=1532544335",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes2_720x.jpg?v=1532544335",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes2_966x.jpg?v=1532544335",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes2_1200x.jpg?v=1532544335",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussettes2_small.jpg?v=1532544335"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 6
        },

        {
          id: "1578005495850",
          description:
            "These ankle socks will get you everywhere you need to go thanks to their easy-to-match design. With exclusive and fun prints created by Rachel, you’ll love these socks that are totally out of the ordinary! Pack of 2One size",
          title: "Graphic Ankle Socks (2 Pack)",
          handle: "2-long-socks-pack",
          available: true,
          price: 1100,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks2_560x.jpg?v=1532543564",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks2_720x.jpg?v=1532543564",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks2_966x.jpg?v=1532543564",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks2_1200x.jpg?v=1532543564",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socks2_small.jpg?v=1532543564"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_3a0805a4-7cb5-40dc-8925-be1aead12400_560x.jpg?v=1532543575",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_3a0805a4-7cb5-40dc-8925-be1aead12400_720x.jpg?v=1532543575",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_3a0805a4-7cb5-40dc-8925-be1aead12400_966x.jpg?v=1532543575",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_3a0805a4-7cb5-40dc-8925-be1aead12400_1200x.jpg?v=1532543575",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_3a0805a4-7cb5-40dc-8925-be1aead12400_small.jpg?v=1532543575"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_8225524d-e566-4377-a911-8993021ebb52_560x.jpg?v=1532543589",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_8225524d-e566-4377-a911-8993021ebb52_720x.jpg?v=1532543589",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_8225524d-e566-4377-a911-8993021ebb52_966x.jpg?v=1532543589",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_8225524d-e566-4377-a911-8993021ebb52_1200x.jpg?v=1532543589",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_8225524d-e566-4377-a911-8993021ebb52_small.jpg?v=1532543589"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_2d6f9167-ea3d-4ae0-99b8-8aef89869e0f_560x.jpg?v=1532543606",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_2d6f9167-ea3d-4ae0-99b8-8aef89869e0f_720x.jpg?v=1532543606",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_2d6f9167-ea3d-4ae0-99b8-8aef89869e0f_966x.jpg?v=1532543606",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_2d6f9167-ea3d-4ae0-99b8-8aef89869e0f_1200x.jpg?v=1532543606",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_2d6f9167-ea3d-4ae0-99b8-8aef89869e0f_small.jpg?v=1532543606"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_a1ca4821-d120-43a6-ba63-83ba9afe02c7_560x.jpg?v=1532543614",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_a1ca4821-d120-43a6-ba63-83ba9afe02c7_720x.jpg?v=1532543614",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_a1ca4821-d120-43a6-ba63-83ba9afe02c7_966x.jpg?v=1532543614",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_a1ca4821-d120-43a6-ba63-83ba9afe02c7_1200x.jpg?v=1532543614",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_a1ca4821-d120-43a6-ba63-83ba9afe02c7_small.jpg?v=1532543614"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 4
        },

        {
          id: "1836092391466",
          description:
            "Stay warm and stylish this winter in these warm tights featuring an all-over chevron print. They’re the perfect option to allow you to keep wearing skirts and dresses all season long. Made in Italy Comfortable and resistant",
          title: "Warm Navy Chevron Tights",
          handle: "warm-navy-chevron-tights",
          available: true,
          price: 2400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_03f375b8-a0fe-482d-94eb-136f6fef4bc2_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_03f375b8-a0fe-482d-94eb-136f6fef4bc2_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_03f375b8-a0fe-482d-94eb-136f6fef4bc2_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_03f375b8-a0fe-482d-94eb-136f6fef4bc2_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/10_03f375b8-a0fe-482d-94eb-136f6fef4bc2_small.jpg?v=1550163941"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_ea22131e-3e16-48fa-82f9-d7766eb2f5cc_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_ea22131e-3e16-48fa-82f9-d7766eb2f5cc_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_ea22131e-3e16-48fa-82f9-d7766eb2f5cc_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_ea22131e-3e16-48fa-82f9-d7766eb2f5cc_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_ea22131e-3e16-48fa-82f9-d7766eb2f5cc_small.jpg?v=1550163941"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_7f186fc9-e6f2-4d64-aca9-bb9935cf0812_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_7f186fc9-e6f2-4d64-aca9-bb9935cf0812_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_7f186fc9-e6f2-4d64-aca9-bb9935cf0812_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_7f186fc9-e6f2-4d64-aca9-bb9935cf0812_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_7f186fc9-e6f2-4d64-aca9-bb9935cf0812_small.jpg?v=1550163941"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1_small.jpg?v=1550163941"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1_small.jpg?v=1550163941"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_560x.jpg?v=1550163941",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_720x.jpg?v=1550163941",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_966x.jpg?v=1550163941",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_1200x.jpg?v=1550163941",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_small.jpg?v=1550163941"
            }
          ],
          type: "Tights",
          average_rating: 5.0,
          reviews_count: 16
        },

        {
          id: "1836153077802",
          description:
            "Beat the cold this winter with leg warmers featuring lurex detailing and add a touch of glitter to your seasonal looks. Made in Italy Comfortable and resistant Note: Height 23 inches",
          title: "Black Glitter Leg Warmers",
          handle: "black-glitter-leg-warmers",
          available: false,
          price: 2000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25-3_560x.jpg?v=1545887612",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25-3_720x.jpg?v=1545887612",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25-3_966x.jpg?v=1545887612",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25-3_1200x.jpg?v=1545887612",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25-3_small.jpg?v=1545887612"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26-2_560x.jpg?v=1545887624",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26-2_720x.jpg?v=1545887624",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26-2_966x.jpg?v=1545887624",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26-2_1200x.jpg?v=1545887624",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26-2_small.jpg?v=1545887624"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 3
        },

        {
          id: "1963714609194",
          description:
            "Don’t wait for life to give you lemons and just snag the Limoncello Necklace instead. The quirky design is made to mimic your favourite citrus and is complete with a solid yellow cord.Made and designed in Montreal by CamilletteAdjustable cord (yellow)Pendant: 27mm x 35mmSingle gold bead detailingColour: Gold",
          title: "Limoncello Necklace (Yellow)",
          handle: "limoncello-necklace-yellow",
          available: false,
          price: 3900,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_560x.png?v=1559329947",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_720x.png?v=1559329947",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_966x.png?v=1559329947",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1200x.png?v=1559329947",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_small.png?v=1559329947"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_560x.png?v=1559329957",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_720x.png?v=1559329957",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_966x.png?v=1559329957",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_1200x.png?v=1559329957",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/22_small.png?v=1559329957"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_560x.png?v=1559329961",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_720x.png?v=1559329961",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_966x.png?v=1559329961",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_1200x.png?v=1559329961",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/21_small.png?v=1559329961"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_560x.png?v=1559329969",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_720x.png?v=1559329969",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_966x.png?v=1559329969",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1200x.png?v=1559329969",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_small.png?v=1559329969"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1963714740266",
          description:
            "Add a summer feel to all your looks with the Spritz Bracelet. The quirky design is made to mimic your favourite citrus and is complete with a solid burnt orange cord.Made and designed in Montreal by CamilletteAdjustable cord (orange)Single gold bead detailingColour: Gold",
          title: "Spritz Bracelet (Orange)",
          handle: "spritz-bracelet",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_560x.png?v=1559337153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_720x.png?v=1559337153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_966x.png?v=1559337153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1200x.png?v=1559337153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_small.png?v=1559337153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_560x.png?v=1559337153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_720x.png?v=1559337153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_966x.png?v=1559337153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_1200x.png?v=1559337153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_small.png?v=1559337153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_560x.png?v=1559337153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_720x.png?v=1559337153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_966x.png?v=1559337153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_1200x.png?v=1559337153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/13_small.png?v=1559337153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_560x.png?v=1559337153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_720x.png?v=1559337153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_966x.png?v=1559337153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1200x.png?v=1559337153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_small.png?v=1559337153"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_560x.png?v=1559337153",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_720x.png?v=1559337153",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_966x.png?v=1559337153",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1200x.png?v=1559337153",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_small.png?v=1559337153"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1963714707498",
          description:
            "Let these earrings become your new daily essential and live everyday like a vacation day. The quirky design is made to mimic your favourite citrus for a fun festive and summer feel. Made and designed in Montreal by Camillette14 mm X 14 mmColour: Gold",
          title: "Bellini Earrings",
          handle: "bellini-earrings",
          available: true,
          price: 5100,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_560x.png?v=1559328792",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_720x.png?v=1559328792",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_966x.png?v=1559328792",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1200x.png?v=1559328792",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_small.png?v=1559328792"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_560x.png?v=1559328807",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_720x.png?v=1559328807",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_966x.png?v=1559328807",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_1200x.png?v=1559328807",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/17_small.png?v=1559328807"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_560x.png?v=1559328818",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_720x.png?v=1559328818",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_966x.png?v=1559328818",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_1200x.png?v=1559328818",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18_small.png?v=1559328818"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_560x.png?v=1559328842",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_720x.png?v=1559328842",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_966x.png?v=1559328842",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_1200x.png?v=1559328842",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_small.png?v=1559328842"
            }
          ],
          type: "Default",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1963714674730",
          description:
            "Don’t wait for life to give you lemons and just snag the Limoncello Necklace instead. The quirky design is made to mimic your favourite citrus and is complete with a solid black cord.Made and designed in Montreal by CamilletteAdjustable cord (black)Pendant: 27mm x 35mmSingle gold bead detailingColour: Gold",
          title: "Limoncello Necklace (Black)",
          handle: "limoncello-necklace-black",
          available: false,
          price: 3900,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_560x.png?v=1559331922",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_720x.png?v=1559331922",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_966x.png?v=1559331922",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_1200x.png?v=1559331922",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/11_small.png?v=1559331922"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_93bab4c4-c7d2-417d-97b4-37237ea361ba_560x.png?v=1559331922",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_93bab4c4-c7d2-417d-97b4-37237ea361ba_720x.png?v=1559331922",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_93bab4c4-c7d2-417d-97b4-37237ea361ba_966x.png?v=1559331922",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_93bab4c4-c7d2-417d-97b4-37237ea361ba_1200x.png?v=1559331922",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19_93bab4c4-c7d2-417d-97b4-37237ea361ba_small.png?v=1559331922"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_560x.png?v=1559331922",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_720x.png?v=1559331922",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_966x.png?v=1559331922",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1200x.png?v=1559331922",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_small.png?v=1559331922"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_560x.png?v=1559331922",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_720x.png?v=1559331922",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_966x.png?v=1559331922",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_1200x.png?v=1559331922",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/20_small.png?v=1559331922"
            }
          ],
          type: "Default",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "1460832862250",
          description:
            "This versatile short has an exclusive pineapple print and is an ideal base layer for active trainings. Featuring a high waisted fit and made of high performance athletic materials, they are 100% made in Quebec. ",
          title: "Pineapple Short",
          handle: "short-ananas",
          available: false,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas_679117f1-ca32-4e1b-be35-1ba0b1adafb7_560x.jpg?v=1529597624",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas_679117f1-ca32-4e1b-be35-1ba0b1adafb7_720x.jpg?v=1529597624",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas_679117f1-ca32-4e1b-be35-1ba0b1adafb7_966x.jpg?v=1529597624",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas_679117f1-ca32-4e1b-be35-1ba0b1adafb7_1200x.jpg?v=1529597624",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas_679117f1-ca32-4e1b-be35-1ba0b1adafb7_small.jpg?v=1529597624"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortAnanas_560x.jpg?v=1529597624",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortAnanas_720x.jpg?v=1529597624",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortAnanas_966x.jpg?v=1529597624",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortAnanas_1200x.jpg?v=1529597624",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortAnanas_small.jpg?v=1529597624"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas2_560x.jpg?v=1529597624",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas2_720x.jpg?v=1529597624",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas2_966x.jpg?v=1529597624",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas2_1200x.jpg?v=1529597624",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortananas2_small.jpg?v=1529597624"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 5
        },

        {
          id: "1836144394282",
          description:
            "Add these boot cuffs to your winter looks to cushion the top of your boots and add additional warmth and style. They’re ideal to prevent snow and rocks from entering your boots and are less bulky than leg warmers.Made in Italy Comfortable and resistant",
          title: "Beige Boot Cuffs",
          handle: "beige-boot-cuffs",
          available: true,
          price: 1300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1f1e7dc4-736c-4935-81ea-884373f87150_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1f1e7dc4-736c-4935-81ea-884373f87150_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1f1e7dc4-736c-4935-81ea-884373f87150_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1f1e7dc4-736c-4935-81ea-884373f87150_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/14_1_1f1e7dc4-736c-4935-81ea-884373f87150_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/16_1_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24-2_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24-2_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24-2_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24-2_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24-2_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_f6fd8908-e688-4f23-ba28-049db867518e_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_f6fd8908-e688-4f23-ba28-049db867518e_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_f6fd8908-e688-4f23-ba28-049db867518e_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_f6fd8908-e688-4f23-ba28-049db867518e_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1_f6fd8908-e688-4f23-ba28-049db867518e_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_9f069d09-9807-438a-bdc9-d283d19f6b3e_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_9f069d09-9807-438a-bdc9-d283d19f6b3e_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_9f069d09-9807-438a-bdc9-d283d19f6b3e_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_9f069d09-9807-438a-bdc9-d283d19f6b3e_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/15_1_9f069d09-9807-438a-bdc9-d283d19f6b3e_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.2_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.2_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.2_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.2_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.2_small.jpg?v=1547326351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.1_560x.jpg?v=1547326351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.1_720x.jpg?v=1547326351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.1_966x.jpg?v=1547326351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.1_1200x.jpg?v=1547326351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffBeige.1_small.jpg?v=1547326351"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 3
        },

        {
          id: "1836148293674",
          description:
            "Add these boot cuffs to your winter looks to cushion the top of your boots and add additional warmth and style. They’re ideal to prevent snow and rocks from entering your boots and are less bulky than leg warmers.Made in Italy Comfortable and resistant",
          title: "Mustard Yellow Boot Cuffs",
          handle: "mustard-yellow-boot-cuffs",
          available: false,
          price: 1300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_560x.jpg?v=1545887311",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_720x.jpg?v=1545887311",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_966x.jpg?v=1545887311",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_1200x.jpg?v=1545887311",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1_small.jpg?v=1545887311"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_560x.jpg?v=1545887320",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_720x.jpg?v=1545887320",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_966x.jpg?v=1545887320",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_1200x.jpg?v=1545887320",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/27_1_small.jpg?v=1545887320"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_8adbe9e3-cf9d-4aac-87fc-b9f26e8fa0a5_560x.jpg?v=1545887327",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_8adbe9e3-cf9d-4aac-87fc-b9f26e8fa0a5_720x.jpg?v=1545887327",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_8adbe9e3-cf9d-4aac-87fc-b9f26e8fa0a5_966x.jpg?v=1545887327",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_8adbe9e3-cf9d-4aac-87fc-b9f26e8fa0a5_1200x.jpg?v=1545887327",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/26_1_8adbe9e3-cf9d-4aac-87fc-b9f26e8fa0a5_small.jpg?v=1545887327"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1_560x.jpg?v=1545887332",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1_720x.jpg?v=1545887332",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1_966x.jpg?v=1545887332",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1_1200x.jpg?v=1545887332",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/25_1_small.jpg?v=1545887332"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_55f83cf7-ec5c-450e-b259-761fcdb3564c_560x.jpg?v=1545887340",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_55f83cf7-ec5c-450e-b259-761fcdb3564c_720x.jpg?v=1545887340",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_55f83cf7-ec5c-450e-b259-761fcdb3564c_966x.jpg?v=1545887340",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_55f83cf7-ec5c-450e-b259-761fcdb3564c_1200x.jpg?v=1545887340",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/23_1_55f83cf7-ec5c-450e-b259-761fcdb3564c_small.jpg?v=1545887340"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-2_560x.jpg?v=1545887351",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-2_720x.jpg?v=1545887351",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-2_966x.jpg?v=1545887351",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-2_1200x.jpg?v=1545887351",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-2_small.jpg?v=1545887351"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18-2_560x.jpg?v=1545887361",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18-2_720x.jpg?v=1545887361",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18-2_966x.jpg?v=1545887361",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18-2_1200x.jpg?v=1545887361",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/18-2_small.jpg?v=1545887361"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1_560x.jpg?v=1545887368",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1_720x.jpg?v=1545887368",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1_966x.jpg?v=1545887368",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1_1200x.jpg?v=1545887368",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/12_1_small.jpg?v=1545887368"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.2_2_560x.jpg?v=1547326574",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.2_2_720x.jpg?v=1547326574",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.2_2_966x.jpg?v=1547326574",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.2_2_1200x.jpg?v=1547326574",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.2_2_small.jpg?v=1547326574"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.1_2_560x.jpg?v=1547326577",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.1_2_720x.jpg?v=1547326577",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.1_2_966x.jpg?v=1547326577",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.1_2_1200x.jpg?v=1547326577",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.1_2_small.jpg?v=1547326577"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.3_2_560x.jpg?v=1547326595",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.3_2_720x.jpg?v=1547326595",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.3_2_966x.jpg?v=1547326595",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.3_2_1200x.jpg?v=1547326595",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/BootCuffMout.3_2_small.jpg?v=1547326595"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 3
        },

        {
          id: "1460788232234",
          description:
            "These fishnet socks feature a solid knit trim and toe bed to fit comfortably in all your favourite footwear.",
          title: "Black Fishnet Socks",
          handle: "black-fishnet-socks",
          available: false,
          price: 1000,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille2_560x.jpg?v=1529683845",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille2_720x.jpg?v=1529683845",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille2_966x.jpg?v=1529683845",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille2_1200x.jpg?v=1529683845",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille2_small.jpg?v=1529683845"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksResilleNoir_519b47f2-7222-4564-82d1-6d81f6100153_560x.jpg?v=1529683845",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksResilleNoir_519b47f2-7222-4564-82d1-6d81f6100153_720x.jpg?v=1529683845",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksResilleNoir_519b47f2-7222-4564-82d1-6d81f6100153_966x.jpg?v=1529683845",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksResilleNoir_519b47f2-7222-4564-82d1-6d81f6100153_1200x.jpg?v=1529683845",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/socksResilleNoir_519b47f2-7222-4564-82d1-6d81f6100153_small.jpg?v=1529683845"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_5fe2511b-81fc-4c09-8270-a601b20685eb_560x.jpg?v=1529683845",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_5fe2511b-81fc-4c09-8270-a601b20685eb_720x.jpg?v=1529683845",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_5fe2511b-81fc-4c09-8270-a601b20685eb_966x.jpg?v=1529683845",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_5fe2511b-81fc-4c09-8270-a601b20685eb_1200x.jpg?v=1529683845",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_5fe2511b-81fc-4c09-8270-a601b20685eb_small.jpg?v=1529683845"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks_560x.jpg?v=1529683845",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks_720x.jpg?v=1529683845",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks_966x.jpg?v=1529683845",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks_1200x.jpg?v=1529683845",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks_small.jpg?v=1529683845"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussetteresille_560x.jpg?v=1529683868",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussetteresille_720x.jpg?v=1529683868",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussetteresille_966x.jpg?v=1529683868",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussetteresille_1200x.jpg?v=1529683868",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/chaussetteresille_small.jpg?v=1529683868"
            }
          ],
          type: "Socks",
          average_rating: 0.0,
          reviews_count: 0
        },

        {
          id: "1369110118442",
          description:
            "These sheer tights have a heart print detailing located on the lower leg to add a subtle flirty touch to all your looks.Now available in extended sizes, these tights are designed with a special waistband that won’t pinch your waist and feature a control panty that will give you a chic fitted look.",
          title: "Back Heart Print Tights (Plus Size)",
          handle: "tights-with-back-heart-print-plus-size",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-compressor_560x.jpg?v=1529681912",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-compressor_720x.jpg?v=1529681912",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-compressor_966x.jpg?v=1529681912",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-compressor_1200x.jpg?v=1529681912",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/19-compressor_small.jpg?v=1529681912"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCoeur_2203b258-3c76-4857-a0f5-8d93fbcb9f5d_560x.jpg?v=1529681912",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCoeur_2203b258-3c76-4857-a0f5-8d93fbcb9f5d_720x.jpg?v=1529681912",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCoeur_2203b258-3c76-4857-a0f5-8d93fbcb9f5d_966x.jpg?v=1529681912",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCoeur_2203b258-3c76-4857-a0f5-8d93fbcb9f5d_1200x.jpg?v=1529681912",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantCoeur_2203b258-3c76-4857-a0f5-8d93fbcb9f5d_small.jpg?v=1529681912"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeursmollet_560x.jpg?v=1529681912",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeursmollet_720x.jpg?v=1529681912",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeursmollet_966x.jpg?v=1529681912",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeursmollet_1200x.jpg?v=1529681912",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantcoeursmollet_small.jpg?v=1529681912"
            }
          ],
          type: "Plus Size Tights",
          average_rating: 4.5,
          reviews_count: 4
        },

        {
          id: "1460833484842",
          description:
            "These leggings have an exclusive pineapple print for a fun finish that’s perfect to complete your summer athleisure wardrobe. Featuring a high waisted fit and made of high performance athletic materials, they are 100% made in Quebec. ",
          title: "Pineapple Leggings",
          handle: "leggings-ananas",
          available: true,
          price: 6800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_560x.jpg?v=1529683164",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_720x.jpg?v=1529683164",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_966x.jpg?v=1529683164",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_1200x.jpg?v=1529683164",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/5_small.jpg?v=1529683164"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingAnanas_560x.jpg?v=1529683164",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingAnanas_720x.jpg?v=1529683164",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingAnanas_966x.jpg?v=1529683164",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingAnanas_1200x.jpg?v=1529683164",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingAnanas_small.jpg?v=1529683164"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_b64edbcb-057b-4baf-b054-b08f3534b1cc_560x.jpg?v=1529683164",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_b64edbcb-057b-4baf-b054-b08f3534b1cc_720x.jpg?v=1529683164",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_b64edbcb-057b-4baf-b054-b08f3534b1cc_966x.jpg?v=1529683164",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_b64edbcb-057b-4baf-b054-b08f3534b1cc_1200x.jpg?v=1529683164",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/7_b64edbcb-057b-4baf-b054-b08f3534b1cc_small.jpg?v=1529683164"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_560x.jpg?v=1529683164",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_720x.jpg?v=1529683164",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_966x.jpg?v=1529683164",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_1200x.jpg?v=1529683164",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_small.jpg?v=1529683164"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_89e43bb7-f8bb-4b72-acdd-b5931d286ccb_560x.jpg?v=1529683164",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_89e43bb7-f8bb-4b72-acdd-b5931d286ccb_720x.jpg?v=1529683164",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_89e43bb7-f8bb-4b72-acdd-b5931d286ccb_966x.jpg?v=1529683164",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_89e43bb7-f8bb-4b72-acdd-b5931d286ccb_1200x.jpg?v=1529683164",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsananas_89e43bb7-f8bb-4b72-acdd-b5931d286ccb_small.jpg?v=1529683164"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 3
        },

        {
          id: "1460791541802",
          description:
            "These black fishnet tights feature a large-sized fishnet stitch and will provide some edge to all your looks!",
          title: "Large Fishnet Tights",
          handle: "large-fishnet-socks",
          available: true,
          price: 1400,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille_560x.jpg?v=1529682959",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille_720x.jpg?v=1529682959",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille_966x.jpg?v=1529682959",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille_1200x.jpg?v=1529682959",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille_small.jpg?v=1529682959"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_560x.jpg?v=1529682959",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_720x.jpg?v=1529682959",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_966x.jpg?v=1529682959",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_1200x.jpg?v=1529682959",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/resille_small.jpg?v=1529682959"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosseresille_560x.jpg?v=1529682959",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosseresille_720x.jpg?v=1529682959",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosseresille_966x.jpg?v=1529682959",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosseresille_1200x.jpg?v=1529682959",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosseresille_small.jpg?v=1529682959"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille2_560x.jpg?v=1529682959",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille2_720x.jpg?v=1529682959",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille2_966x.jpg?v=1529682959",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille2_1200x.jpg?v=1529682959",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/grosseresille2_small.jpg?v=1529682959"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille_560x.jpg?v=1529682983",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille_720x.jpg?v=1529682983",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille_966x.jpg?v=1529682983",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille_1200x.jpg?v=1529682983",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille_small.jpg?v=1529682983"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille3_560x.jpg?v=1529683110",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille3_720x.jpg?v=1529683110",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille3_966x.jpg?v=1529683110",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille3_1200x.jpg?v=1529683110",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantgrosresille3_small.jpg?v=1529683110"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 4
        },

        {
          id: "1702273351722",
          description:
            "These semi-sheer tights are a classic in any wardrobe. With this pack, you’ll get 3 pairs of tights for the price of 2!",
          title: "Semi-sheer tights 30D (3 pack)",
          handle: "pack-3-collants-semi-transparents-30d",
          available: true,
          price: 2700,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_44efdac5-163f-4656-b92a-d50c587dd3a0_560x.jpg?v=1538081407",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_44efdac5-163f-4656-b92a-d50c587dd3a0_720x.jpg?v=1538081407",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_44efdac5-163f-4656-b92a-d50c587dd3a0_966x.jpg?v=1538081407",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_44efdac5-163f-4656-b92a-d50c587dd3a0_1200x.jpg?v=1538081407",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/collantsemitransparent_44efdac5-163f-4656-b92a-d50c587dd3a0_small.jpg?v=1538081407"
            }
          ],
          type: "Tights",
          average_rating: 4.5,
          reviews_count: 4
        },

        {
          id: "1416438317098",
          description:
            "The pretty details on these mesh ruching leggings make them ideal for wearing to a get-together with your girls, but also allow you the flexibility to be unhindered in your weekly yoga sessions.",
          title: "Ankle Mesh Legging",
          handle: "ankle-mesh-legging",
          available: false,
          price: 6300,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging-sort_560x.jpg?v=1529683647",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging-sort_720x.jpg?v=1529683647",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging-sort_966x.jpg?v=1529683647",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging-sort_1200x.jpg?v=1529683647",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging-sort_small.jpg?v=1529683647"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_face_560x.jpg?v=1529683647",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_face_720x.jpg?v=1529683647",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_face_966x.jpg?v=1529683647",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_face_1200x.jpg?v=1529683647",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_face_small.jpg?v=1529683647"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_560x.jpg?v=1529683647",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_720x.jpg?v=1529683647",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_966x.jpg?v=1529683647",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_1200x.jpg?v=1529683647",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_resille_small.jpg?v=1529683647"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_560x.jpg?v=1529683647",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_720x.jpg?v=1529683647",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_966x.jpg?v=1529683647",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_1200x.jpg?v=1529683647",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/24_small.jpg?v=1529683647"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 4
        },

        {
          id: "1460833419306",
          description:
            "This versatile short has an exclusive floral print and is an ideal base layer for active trainings. Featuring a high waisted fit and made of high performance athletic materials, they are 100% made in Quebec. ",
          title: "Floral Short",
          handle: "short-floral",
          available: true,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_faab2b14-140b-468a-a4f4-a07dc8ea51c7_560x.jpg?v=1529597584",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_faab2b14-140b-468a-a4f4-a07dc8ea51c7_720x.jpg?v=1529597584",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_faab2b14-140b-468a-a4f4-a07dc8ea51c7_966x.jpg?v=1529597584",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_faab2b14-140b-468a-a4f4-a07dc8ea51c7_1200x.jpg?v=1529597584",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/9_faab2b14-140b-468a-a4f4-a07dc8ea51c7_small.jpg?v=1529597584"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortfleur_560x.jpg?v=1529597584",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortfleur_720x.jpg?v=1529597584",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortfleur_966x.jpg?v=1529597584",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortfleur_1200x.jpg?v=1529597584",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/shortfleur_small.jpg?v=1529597584"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_560x.jpg?v=1529597584",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_720x.jpg?v=1529597584",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_966x.jpg?v=1529597584",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_1200x.jpg?v=1529597584",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/8_small.jpg?v=1529597584"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "1460834467882",
          description:
            "These leggings have an exclusive floral print for a fun finish that’s perfect to complete your summer athleisure wardrobe. Featuring a high waisted fit and made of high performance athletic materials, they are 100% made in Quebec.",
          title: "Floral Leggings",
          handle: "leggings-floral",
          available: true,
          price: 6800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_560x.jpg?v=1529683373",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_720x.jpg?v=1529683373",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_966x.jpg?v=1529683373",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_1200x.jpg?v=1529683373",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/3_small.jpg?v=1529683373"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingFleurs_560x.jpg?v=1529683373",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingFleurs_720x.jpg?v=1529683373",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingFleurs_966x.jpg?v=1529683373",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingFleurs_1200x.jpg?v=1529683373",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingFleurs_small.jpg?v=1529683373"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsfleur_560x.jpg?v=1529683373",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsfleur_720x.jpg?v=1529683373",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsfleur_966x.jpg?v=1529683373",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsfleur_1200x.jpg?v=1529683373",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsfleur_small.jpg?v=1529683373"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_560x.jpg?v=1529683373",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_720x.jpg?v=1529683373",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_966x.jpg?v=1529683373",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_1200x.jpg?v=1529683373",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/4_small.jpg?v=1529683373"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 3
        },

        {
          id: "1460834435114",
          description:
            "These leggings have an exclusive tropical print for a fun finish that’s perfect to complete your summer athleisure wardrobe. Featuring a high waisted fit and made of high performance athletic materials, they are 100% made in Quebec.",
          title: "Tropical Leggings",
          handle: "leggings-tropical",
          available: true,
          price: 6800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_560x.jpg?v=1529597519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_720x.jpg?v=1529597519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_966x.jpg?v=1529597519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_1200x.jpg?v=1529597519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/2_small.jpg?v=1529597519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingTropic_560x.jpg?v=1529597519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingTropic_720x.jpg?v=1529597519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingTropic_966x.jpg?v=1529597519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingTropic_1200x.jpg?v=1529597519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingTropic_small.jpg?v=1529597519"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingstropical_560x.jpg?v=1529597519",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingstropical_720x.jpg?v=1529597519",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingstropical_966x.jpg?v=1529597519",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingstropical_1200x.jpg?v=1529597519",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingstropical_small.jpg?v=1529597519"
            }
          ],
          type: "Leggings",
          average_rating: 4.5,
          reviews_count: 2
        },

        {
          id: "1460787904554",
          description:
            "These fishnet socks feature a solid knit trim and toe bed to fit comfortably in all your favourite footwear.",
          title: "Lilac Fishnet Socks",
          handle: "pink-fishnet-socks",
          available: true,
          price: 800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sockResillePink_560x.jpg?v=1525977390",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sockResillePink_720x.jpg?v=1525977390",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sockResillePink_966x.jpg?v=1525977390",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sockResillePink_1200x.jpg?v=1525977390",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/sockResillePink_small.jpg?v=1525977390"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks2_560x.jpg?v=1526671552",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks2_720x.jpg?v=1526671552",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks2_966x.jpg?v=1526671552",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks2_1200x.jpg?v=1526671552",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/fishnetsocks2_small.jpg?v=1526671552"
            }
          ],
          type: "Socks",
          average_rating: 5.0,
          reviews_count: 2
        },

        {
          id: "1478566838314",
          description:
            "Your favourite summer print has bloomed on the perfect white t-shirt this season! Featuring an exclusive, yet casual design From Rachel, and a round collar, this ultra comfortable t-shirt will take you everywhere this season.",
          title: "Wild Flower Summer Tee",
          handle: "summer-t-shirt",
          available: true,
          price: 2800,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_560x.jpg?v=1530651035",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_720x.jpg?v=1530651035",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_966x.jpg?v=1530651035",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_1200x.jpg?v=1530651035",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/29_small.jpg?v=1530651035"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_560x.jpg?v=1530651043",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_720x.jpg?v=1530651043",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_966x.jpg?v=1530651043",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_1200x.jpg?v=1530651043",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/30_small.jpg?v=1530651043"
            }
          ],
          type: "Apparel",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "806981828650",
          description:
            "This legging has mesh inserts around the leg for an athleisure look that’s also trendy. Features a wide ultra comfort waistband so you’ll be comfortable too!",
          title: "Legging with Mesh Inserts",
          handle: "leggings-with-mesh-inserts",
          available: false,
          price: 3500,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_face_560x.jpg?v=1522703346",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_face_720x.jpg?v=1522703346",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_face_966x.jpg?v=1522703346",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_face_1200x.jpg?v=1522703346",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_face_small.jpg?v=1522703346"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_dos_560x.jpg?v=1522703350",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_dos_720x.jpg?v=1522703350",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_dos_966x.jpg?v=1522703350",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_dos_1200x.jpg?v=1522703350",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/legging_rayure_resille_dos_small.jpg?v=1522703350"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggings2-1_560x.jpg?v=1522703371",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggings2-1_720x.jpg?v=1522703371",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggings2-1_966x.jpg?v=1522703371",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggings2-1_1200x.jpg?v=1522703371",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggings2-1_small.jpg?v=1522703371"
            },

            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsresille_560x.jpg?v=1522703382",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsresille_720x.jpg?v=1522703382",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsresille_966x.jpg?v=1522703382",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsresille_1200x.jpg?v=1522703382",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/leggingsresille_small.jpg?v=1522703382"
            }
          ],
          type: "Leggings",
          average_rating: 5.0,
          reviews_count: 1
        },

        {
          id: "1543284588586",
          description:
            "This is the perfect scrub for taking care of your legs this summer. They won't ever get this soft!  Fragrance developed exclusively with Be-U Cosmetics in Montreal. VeganNot tested on animalsParaben and sulfate freeHypoallergenic ",
          title: "Scrub",
          handle: "scrub",
          available: true,
          price: 1100,
          images: [
            {
              sm:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/scrub_560x.jpg?v=1530125149",
              md:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/scrub_720x.jpg?v=1530125149",
              lg:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/scrub_966x.jpg?v=1530125149",
              xl:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/scrub_1200x.jpg?v=1530125149",
              original:
                "https://cdn.shopify.com/s/files/1/2592/7300/products/scrub_small.jpg?v=1530125149"
            }
          ],
          type: "Default",
          average_rating: 5.0,
          reviews_count: 1
        }
      ]),
    2000
  )
);


