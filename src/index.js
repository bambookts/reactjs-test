import React from "react";
import ReactDOM from "react-dom";
import App from './components/App.jsx';

/* METHOD TO ASYNC FETCH THE PRODUCTS */
// import getProducts from "./getProducts";

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css' // Import precompiled Bootstrap css

ReactDOM.render(
  <App/>,
  document.getElementById("app")
);

